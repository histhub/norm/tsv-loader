#!/usr/bin/env python3

import logging
import sys
from argparse import ArgumentParser

import eavlib.graph
import eavlib.connection
from eavlib.postgres.factory_function import build_eav_connection

from tsv_loader.tsv_loader import TsvLoader
from tsv_loader.parsers.histhub_dates_parser import histhub_dates_parser
from tsv_loader.parsers.histhub_durations_parser import histhub_durations_parser
from tsv_loader.parsers.histhub_names_parser import histhub_forenames_parser


if __name__ == '__main__':

    args_parser = ArgumentParser(description='Loads TSV files into an EAV repository')
    args_parser.add_argument(
        'connection',
        help     = 'database connection string in the form "{engine}://{user}@{host}:{port}/{database}.{schema}"'
    )
    args_parser.add_argument(
        '-f', '--file',
        required = True,
        action   = 'store',
        dest     = 'plan_file',
        help     = 'load plan file describing how the tabular data can be '
                   'transformed into a graph structure'
    )
    args_parser.add_argument(
        '-g', '--log',
        required = False,
        action   = 'store',
        choices  = ['CRITICAL', 'FATAL', 'ERROR', 'WARN', 'WARNING', 'INFO', 'DEBUG', 'NOTSET'],
        default  = 'INFO',
        dest     = 'log_level',
        help     = 'logging level to be used'
    )
    args_parser.add_argument(
        '-m', '--model',
        required = False,
        action   = 'store',
        dest     = 'model',
        help     = 'model name'
    )
    args_parser.add_argument(
        '-i', '--dbid',
        required = False,
        action   = 'store',
        dest     = 'dbid',
        help     = "database ID attribute name"
    )
    args = args_parser.parse_args()

    logging.basicConfig(
        format='%(levelname)-8s %(asctime)s %(message)s',
        level=logging._nameToLevel[args.log_level])

    try:
        graph = eavlib.graph.Graph(
            eavlib.connection.EAVConnection.fromstr(args.connection),
            args.model)
        TsvLoader(
            graph,
            args.plan_file,
            {
                'histhub_dates_parser': histhub_dates_parser(),
                'histhub_intervals_parser': histhub_durations_parser(),
                'histhub_forenames_parser': histhub_forenames_parser()
            },
            dbid_attr=args.dbid)


    except Exception as e:
        logging.exception(str(e), exc_info=True)
        exit(1)
    finally:
        logging.shutdown()
