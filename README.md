# TSV-Loader

The TSV-Loader is a tool to load batch data into an
[eavlib](https://gitlab.com/histhub/norm/eavlib.git)-compatible graph.

The input is a set of TSV files and a Load-Plan file which describes how the
tabular TSV data can be transformed into a graph structure.

## Installation

The TSV-Loader can be installed like a regular Python package using pip:
```console
$ pip install tsv_loader@git+https://gitlab.com/histhub/norm/tsv-loader.git
```

## Loading data using TSV-Loader

For data to be loadable using TSV-Loader it needs to be "serialised" into a set
of TSV (tab-separated values) files.
The structure of the TSV files is usually similar to how SQL tables would be
layed out.

Once the TSV files are ready, a load plan needs to be written.
The load plan format is described in
[the wiki](https://gitlab.com/histhub/norm/tsv-loader/wikis).

Then, the data can be loaded using:
```console
$ eav_load_tsv postgresql://postgres@localhost/histhub.data -f tsv_load_plan -m histHub
```
**Note:** The database URL is a
[SQLAlchemy-like](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.create_engine)
URL string with extended `database.schema` notation.
