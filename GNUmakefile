# Variables
SRC_DIR := ./tsv_loader
# MIN_PYTHON_VERSION: major.minor.micro allowed
MIN_PYTHON_VERSION := 3.4
VENV := ./venv

# Check if Python binary matches minimum required version
# Usage: $(call is_min_python_version,/path/to/python,min_version_str)
# prints 1 if greater or equal, nothing
define is_min_python_version
$(shell echo $(2) | $(1) -c 'import string,sys;split=(hasattr(str,"split") and str.split or string.splitfields);atoi=(hasattr(string,"atoi") and string.atoi or int);tolist=lambda _:(type([])==type(_) and _ or list(_));str2ver=lambda _:tolist(map(atoi,split(_,".")));sys.stdout.write("1"*(str2ver(split(sys.version," ")[0])>=str2ver(sys.stdin.readline()[:-1])) or sys.exit(1))')
endef

# Get version of Python binary (e.g. 1.4, 2.7.15, 3.7.0)
# Usage: $(call python_version,/path/to/python)
define python_version
$(shell $(1) -c 'import string,sys;split=(hasattr(str,"split") and str.split or string.split);sys.stdout.write(split(sys.version)[0])')
endef

# Path detection
BASE_DIR := $(shell dirname '$(realpath $(lastword $(MAKEFILE_LIST)))')
ifneq ($(BASE_DIR),$(shell pwd -P))
# Ensure that all commands are executed in the BASE_DIR.
%: ; $(MAKE) -C '$(BASE_DIR)' $@
else


# Determine which Python binary to use
ifneq ($(PYTHON),)
SYSTEMPYTHON := '$(PYTHON)'
else
ifeq (3,$(word 1,$(subst ., ,$(MIN_PYTHON_VERSION))))
SYSTEMPYTHON := '$(firstword $(PYTHON) $(shell command -v python3 python ;))'
else
SYSTEMPYTHON := '$(firstword $(PYTHON) $(shell command -v python python2 python3 ;))'
endif
endif

VENVPYTHON := $(VENV)/bin/python

# Unset PYTHONHOME environment variable, because that's what a virtualenv's
# activate script does, too
unexport PYTHONHOME

# Helper command to install using pip into the virtualenv
PIP_INSTALL = \
$(if $(CFLAGS),CFLAGS='$(CFLAGS)') \
$(if $(LDFLAGS),LDFLAGS='$(LDFLAGS)') \
$(if $(ARCHFLAGS),ARCHFLAGS='$(ARCHFLAGS)') \
$(VENVPYTHON) -m pip install $(PIPFLAGS) --disable-pip-version-check


# Help

.PHONY: help
help:
	$(info )
	$(info You can choose between these targets:)
	$(info )
	$(info * help				Display this help page.)
	$(info )
	$(info Virtual environment:)
	$(info * virtualenv			Create a venv.)
	$(info * init				Initialize venv (install application requirements).)
	$(info * clean				Clean up.)
	$(info )
	$(info Development:)
	$(info * lint				Run the linter.)
	$(info * test				Run the tests.)
	$(info )
	@: # noop (needed to suppress "nothing to be done")


# Virtualenv targets

$(VENV):
ifndef SYSTEMPYTHON
	$(error Cannot find Python. Please make sure that Python is in PATH, or set
	        the PYTHON envionment variable to the path to your Python
	        interpreter.)
endif
# Check Python version
ifeq ($(call is_min_python_version,$(SYSTEMPYTHON),$(MIN_PYTHON_VERSION)),)
	$(error Python version $(MIN_PYTHON_VERSION) required. Only $(call python_version,$(SYSTEMPYTHON)) found)
endif
# Configure virtualenv
ifneq ($(call is_min_python_version,$(SYSTEMPYTHON),3.3),)
# Use venv module if possible
	$(SYSTEMPYTHON) -m venv '$(VENV)'
else
# Fall back to virtualenv
	'$(firstword $(VIRTUALENV) $(shell command -v virtualenv ;))' -p $(SYSTEMPYTHON) '$(VENV)'
endif

.PRECIOUS: $(VENV)/.%_reqs
$(VENV)/.%_reqs: requirements.d/%.txt | $(VENV)
# Install requirements for %
	$(PIP_INSTALL) -r './requirements.d/$*.txt'
	@touch '$@'


# PHONY targets

.PHONY: virtualenv
virtualenv: | $(VENV)
	$(info Created virtualenv in "$(realpath $(VENV))".)

.PHONY: init
init: | $(VENV)/.app_reqs ;


.PHONY: clean
clean:
	$(RM) -R '$(VENV)'
	find '$(SRC_DIR)' -iname '*.pyc' -delete
	find '$(SRC_DIR)' -type d -name '__pycache__' -delete

.PHONY: lint
lint: $(VENV)/.development_reqs
	$(VENVPYTHON) -m flake8 --show-source '$(SRC_DIR)'

.PHONY: test
test: $(VENV)/.app_reqs
	$(VENVPYTHON) -m unittest discover --verbose '$(SRC_DIR)'

endif
