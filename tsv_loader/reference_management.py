import logging

from collections import namedtuple

LOG = logging.getLogger(__name__)


class CacheKey(namedtuple('CacheKey', ('entity', 'text'))):
    """Keys to locate cached objects using strings or ids delivered by
    providers.
    """

    def __str__(self):
        return "{}[{}]".format(self.entity, self.text)


class UsageRecord:
    """List of definitions and references of each cache key."""
    __slots__ = ('definitions', 'references')

    def __init__(self):
        self.definitions = []  # definitions are unique
        self.references = set()


##
#  Position where a provider ID was used or defined.
#
UsageLocator = namedtuple('UsageLocator', [
    'cache_key',  # combination of entity name and id provided by submitter
    'file',       # filename where the provided id was used or defined
    'line',       # line number to deliver precise warning or error messages
    'colval',     # ColValue object to deliver precise warning or error msgs.
    ])


class IdUsageTracker:
    """Tracks every use of a provider id to detect definitions without
    references (warnings), or references without definitions (errors).
    """

    def __init__(self):
        self.usage_records = {}
        self.current_file = None

    def set_current_file(self, file_name):
        self.current_file = file_name

    def id_was_defined(self, cache_key, line, colval):
        """Records an ID definition.

        A provider id can be defined more than once since information about an
        entity can be spread across several files. Inside a file a provider id
        can only be defined once.
        """
        if cache_key not in self.usage_records:
            self.usage_records[cache_key] = UsageRecord()
        usage_record = self.usage_records[cache_key]

        for definition in usage_record.definitions:
            if definition.file == self.current_file:
                raise RuntimeError("%s defined more than once" % cache_key)

        usage_record.definitions.append(UsageLocator(
            cache_key, self.current_file, line, colval))

    def id_was_referenced(self, cache_key, line=None, colval=None):
        if cache_key not in self.usage_records:
            self.usage_records[cache_key] = UsageRecord()

        self.usage_records[cache_key].references.add(UsageLocator(
            cache_key,
            self.current_file,
            line if colval.is_column() else None,
            colval))

    def referenced_but_not_defined(self):
        return sum((
            list(u.references) for u in self.usage_records.values()
            if u.references and not u.definitions), [])

    def defined_but_not_referenced(self):
        return sum((
            u.definitions for u in self.usage_records.values()
            if u.definitions and not u.references), [])

    def dump(self):
        for cache_key, record in self.usage_records.items():
            LOG.debug("{}[{}]".format(cache_key.entity, cache_key.text))
            for d in record.definitions:
                LOG.debug("  DEF {} L{}:c{} '{}'".format(
                    d.cache_key, d.line, d.colval.loc, d.file))
            for r in record.references:
                LOG.debug("  REF {} L{}:c{} '{}'".format(
                    r.cache_key, r.line, r.colval.loc, r.file))
