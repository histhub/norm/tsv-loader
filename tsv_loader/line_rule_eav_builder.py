from collections import namedtuple
from eavlib.core import EavBag

from tsv_loader import LoadPlanError
from tsv_loader.load_plan_visitor import LoadPlanVisitor
from tsv_loader.eav_builder_stack import EavBuilderStack
from tsv_loader.reference_management import (CacheKey, IdUsageTracker)

import logging
LOG = logging.getLogger(__name__)


# TODO: remove self._stack.append_missed_value(self._make_error(msg, colval))
# TODO: rename ValueCol( val=... col=... ) could also be ColValue with fields col=... val=...
class ColValue(namedtuple('ColValue', ('val', 'loc'))):
    """Value of a field tied to how it was obtained (column number or literal
    string).
    """
    def is_empty(self):
        return not self.val

    def is_valid(self):
        return not self.is_empty()

    def is_literal(self):
        return self.loc == 0

    def is_column(self):
        return not self.is_literal()


class LineRuleEavBuilder(LoadPlanVisitor):
    """Visitor to build EAV nodes from information extracted from a line of a
    TSV file by traversing a TSV line action rule.

    An internal cache keeps track of objects that are mentioned
    in several files (keyed by provider identifiers). All nodes
    get an EAV ID that gets an entry in the cache for those re-
    used (that is defined as LOAD Entity[n]... in the load plan
    specification).

    Additionally, provider IDs definitions and uses are tracked
    to be able to report inconsistencies among TSV files.

    After a LineActionRule is traversed with an instance of this
    class, the field 'result' contains the built EAV node with
    the current line and the used rule:

        for line in file:
            for rule in file_rules:
                rule.traverse_with( line_rule_eav_builder )
                eav_node = line_rule_eav_builder.result
    """

    ERROR_MSG_FILE = "'{}': {}"
    ERROR_MSG_LINE = ERROR_MSG_FILE + " L{}[{}]"

    def __init__(self, id_generator, parsers_dict, dbid_attr):
        self._id_generator = id_generator
        self._id_iter = iter(())

        self._parsers_dict = parsers_dict
        self.dbid_attr = dbid_attr  # database ID attribute's name

        self._stack = EavBuilderStack()
        self._ids_cache = {}
        self._normal_values = {}
        self._usage_tracker = IdUsageTracker()
        self._build_errors = []

        self._clear_state()

    def __next_id(self):
        try:
            return next(self._id_iter)
        except StopIteration:
            # batch size: 1000
            self._id_iter = self._id_generator.take(1000)
            return self.__next_id()

    def set_current_file(self, file_name):
        self._current_file = file_name
        self._usage_tracker.set_current_file(file_name)

    def set_current_line(self, line):
        self._current_line = line

    def report_build_errors(self, logger):
        for msg in sorted(self._build_errors):
            logger.error(msg)

    def report_reference_usage(self, logger):
        """Issues an error for each provider identifier that was used
        (referenced) but never defined, and a warning for provider identifier
        defined but never used.
        """
        self.__dump_ids_cache()
        for use in sorted(self._usage_tracker.referenced_but_not_defined()):
            err_msg = "{} used but never defined".format(use.cache_key)
            logger.error(self._make_error(
                err_msg, use.colval, use.line, use.file))
        for use in sorted(self._usage_tracker.defined_but_not_referenced()):
            err_msg = "{} defined but never used".format(use.cache_key)
            logger.warning(self._make_error(
                err_msg, use.colval, use.line, use.file))

    def has_errors(self):
        return self._build_errors \
            or self._usage_tracker.referenced_but_not_defined()

    def _clear_state(self):
        self._stack.clear()
        self.result = None

    def _make_error(self, msg, colval, lin=None, filename=None):
        """Creates either a file or line error string out of its components."""
        if filename is None:
            filename = self._current_file

        if colval.is_literal():
            return self.ERROR_MSG_FILE.format(filename, msg)
        else:
            if lin is None:
                lin = self._current_line.line_num
            return self.ERROR_MSG_LINE.format(filename, msg, lin, colval.loc)

    def _empty_instance_reference(self, class_name, value):
        msg = "empty instance reference {}[]".format(class_name)
        self._build_errors.append(self._make_error(msg, value))
        self._stop_traversal()

    def _get_cached_id(self, class_name, colval, **kwargs):
        """Returns a cached id, creating a new one in case of cache miss."""
        if not colval.is_valid():
            raise LoadPlanError("empty cache value")

        cache_key = CacheKey(class_name, colval.val)
        object_id = self._ids_cache.get(cache_key, None)
        if object_id is None:
            object_id = self.__next_id()
            self._ids_cache[cache_key] = object_id

        is_definition = bool(kwargs.get('is_definition', False))
        line_num = self._current_line.line_num
        if is_definition:
            try:
                self._usage_tracker.id_was_defined(cache_key, line_num, colval)
            except RuntimeError as e:
                self._build_errors.append(self._make_error(e, colval))
                self._stop_traversal()
        else:
            self._usage_tracker.id_was_referenced(cache_key, line_num, colval)

        return object_id

    def _prepare_result(self):
        last_record = self._stack.top_record()
        if last_record.parse_errors or last_record.missed_values:
            self._build_errors.extend(last_record.parse_errors)
            self._build_errors.extend(last_record.missed_values)
            self.result = None
        else:
            self.result = last_record.eav_node

    def _rule_value(self, value_spec):
        """Returns the value a rule is working with, that is, either the
        content of a field of the current line if the value specification
        is an integer, or the value spec itself in the other case.
        """
        if type(value_spec) is int:
            field_value = self._current_line[value_spec].strip()
            return ColValue(field_value, value_spec)
        else:
            return ColValue(value_spec, 0)

    ###################     Load Plan Visitor Interface     ###################

    # def previsit(self, node):
    #     LOG.debug("PRE  {}".format(node.__class__.__name__))
    #     super().previsit(node)
    #     self._stack.dump()
    # def postvisit(self, node):
    #     LOG.debug("post {}".format(node.__class__.__name__))
    #     super().postvisit(node)

    def _previsit_building_rule(self, rule):
        """CASE 3 IS 'owner' LOAD Car[2] ( ... ) INTO Person[1].cars"""
        self._clear_state()
        self._stack.push_record()

    def _previsit_linking_rule(self, rule):
        """CASE 3 IS 'owner' LINK Car[2] INTO Person[1].cars"""
        self._clear_state()
        self._stack.push_record()

    def _postvisit_building_rule(self, rule):
        """CASE 3 IS 'owner' LOAD Car[2] ( ... ) INTO Person[1].cars"""
        self._stack.pop_record()
        self._prepare_result()

    def _postvisit_linking_rule(self, rule):
        """CASE 3 IS 'owner' LINK Car[2] INTO Person[1].cars"""
        self._stack.pop_record()
        self._prepare_result()

    def _previsit_normalizing_rule(self, rule):
        """NORMALIZE 1 AS 2"""
        variant = self._rule_value(rule.variant)
        standard = self._rule_value(rule.standard)

        if variant.is_empty() or standard.is_empty():
            if variant.is_empty():
                msg = "empty variant value"
                self._build_errors.append(self._make_error(msg, variant))
            if standard.is_empty():
                msg = "empty standard value"
                self._build_errors.append(self._make_error(msg, standard))
            self._stop_traversal()

        if variant.val in self._normal_values and \
                self._normal_values[variant.val] != standard.val:
            msg = "normalized value redefinition [{}] = {}".format(
                variant.val, standard.val)
            self._build_errors.append(self._make_error(msg, variant))
            self._stop_traversal()
        else:
            self._normal_values[variant.val] = standard.val
        self.result = None

    def _previsit_condition(self, condition):
        """
            CASE 4 IS NOT 'birth'

            Missing values in a conditions are not a problem!
        """
        colval = self._rule_value(condition.col_number)
        condition_fails = colval.val not in condition.allowed_values
        if condition.is_negated:
            condition_fails = not condition_fails
        if condition_fails:
            self._stop_traversal()

    def _previsit_cached_parent_rule(self, rule):
        """
            ... INTO Person[1].cars
        """
        colval = self._rule_value(rule.cache_key)
        if colval.is_valid():
            object_id = self._get_cached_id(rule.class_name, colval)
            self._stack.set_node(EavBag(rule.class_name, object_id))
            self._stack.push_record(rule.relation_name, False)
        else:
            self._empty_instance_reference(rule.class_name, colval)

    def _previsit_cached_child_rule(self, rule):
        """LINK Car[2] ..."""
        colval = self._rule_value(rule.cache_key)
        if colval.is_valid():
            object_id = self._get_cached_id(rule.class_name, colval)
            self._stack.set_node(EavBag(rule.class_name, object_id))
        else:
            self._empty_instance_reference(rule.class_name, colval)

    def _previsit_instance_rule(self, rule):
        """Person[1] ( name = ..."""
        if rule.cache_key is None:
            object_id = self.__next_id()  # TODO: might be empty!
        else:
            colval = self._rule_value(rule.cache_key)
            if colval.is_valid():
                object_id = self._get_cached_id(
                    rule.class_name, colval, is_definition=True)
            else:
                self._empty_instance_reference(rule.class_name, colval)
                object_id = None

        self._stack.set_node(EavBag(rule.class_name, object_id))

    def _previsit_attribute_rule(self, rule):
        """
            occupation = 4
            occupation = 'Carpenter'
            occupation = |4|
            occupation = |'Carpenter'|
        """
        colval = self._rule_value(rule.value_spec)
        if colval.is_valid():
            if rule.is_normalized:
                if colval.val not in self._normal_values:
                    msg = "missing normalized value for '{}'".format(colval.val)
                    self._stack.append_missed_value(self._make_error(msg, colval))
                else:
                    value = self._normal_values[colval.val]
                    self._stack.set_attribute(rule.attribute_name, value)
            else:
                self._stack.set_attribute(rule.attribute_name, colval.val)
        elif not rule.is_optional:
            msg = "missing '{}' attribute value".format(rule.attribute_name)
            self._stack.append_missed_value(self._make_error(msg, colval))

    def _previsit_build_target_rule(self, rule):
        """
            spouse = Person(...)
        """
        self._stack.push_record(rule.relation_name, rule.is_optional)

    def _postvisit_build_target_rule(self, rule):
        """
            spouse = Person(...)
        """
        self._stack.pop_record()

    def _previsit_cached_target_rule(self, rule):
        """
            spouse = Person[4]   # 4th column is some cache key
        """
        colval = self._rule_value(rule.cache_key)
        if colval.is_valid():
            object_id = self._get_cached_id(rule.class_name, colval)
            target_node = EavBag(rule.class_name, object_id)
            self._stack.set_target(rule.relation_name, target_node)
        elif not rule.is_optional:
            msg = "empty instance reference {}[]".format(rule.class_name)
            self._stack.append_missed_value(self._make_error(msg, colval))

    def _previsit_database_target_rule(self, rule):
        """
            spouse = Person<4>   # 4th column is a database ID
        """
        colval = self._rule_value(rule.database_ref)
        if colval.is_valid():
            object_id = self.__next_id()
            db_ref_node = EavBag(rule.class_name, object_id)
            db_ref_node.set_attribute(self.dbid_attr, colval.val)
            self._stack.set_target(rule.relation_name, db_ref_node)
        elif not rule.is_optional:
            msg = "empty instance reference {}<>".format(rule.class_name)
            self._stack.append_missed_value(self._make_error(msg, colval))

    def _previsit_parse_target_rule(self, rule):
        """
            birth = Date { PARSE 4 WITH 'parser_name' ( mode='ISO' lang=3 ) }
            # Date built parsing 4th column with 2 parameters:
        """
        if rule.params:
            self._stack.push_record()

    def _postvisit_parse_target_rule(self, rule):
        """
            birth = Date { PARSE 4 WITH 'parser_name' ( mode='ISO' lang=3 ) }
            # Date built parsing 4th column with 2 parameters:
        """
        colval = self._rule_value(rule.col_number)
        if colval.is_valid():
            params = {}
            if rule.params:
                params_node = self._stack.top_record().eav_node
                params.update(params_node.attributes)
                params.update(((k, l[0])
                               for k, l in params_node.relations.items() if l))
                self._stack.pop_record()

            try:
                field_parser = self._parsers_dict[rule.parser_name]
                parse_result = field_parser.parse(colval.val, params)
                if isinstance(parse_result, EavBag):
                    parsed_nodes = [parse_result]
                else:
                    parsed_nodes = parse_result
                for node in parsed_nodes:
                    node.set_eav_ids(self._id_generator)  # TODO: could be omitted
                    self._stack.set_target(rule.relation_name, node)
            except RuntimeError:
                self._stack.append_parse_error(self._make_error(
                    "cannot build a {} from '{}'".format(
                        rule.class_name, colval.val),
                    colval))
        elif not rule.is_optional:
            self._stack.append_missed_value(self._make_error(
                "cannot parse a {} from an empty string".format(
                    rule.class_name),
                colval))

    ###################     Debugging helper     #############################

    def __dump_ids_cache(self):
        for pid, dbid in self._ids_cache.items():
            LOG.debug("IDs cache  %9d -> '%s|%s'", dbid, pid.entity, pid.text)
