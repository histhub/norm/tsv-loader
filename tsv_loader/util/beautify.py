def beautify_namedtuple(input_text, tab=4):
    """ Transforms the usual string representation for a namedtuple into a
    more readable indented one.

    Input example:
    --------------------------------------------------------------------------
    Build(file='beta.q', name='titles', targets=[Target(id=63, type='basic')])
    --------------------------------------------------------------------------

    Output:
    --------------------------------------------------------------------------
    Build(
        file = 'beta.q',
        name = 'titles',
        targets = [
            Target(
                id = 63,
                type = 'basic'
            )
        ]
    )
    --------------------------------------------------------------------------
    """

    default_tab = 4
    # XXX: Dafuq?
    try:
        if tab < 1:
            tab = default_tab
    except Exception:
        tab = default_tab

    output_text = []
    indent_level = 0

    prev_char = None
    for char in input_text:
        if char in ('(', '['):
            output_text.append(char)
            indent_level += tab
            output_text.append('\n')
            output_text.append(' '*indent_level)

        elif char in (')', ']'):
            indent_level -= tab
            if char == ']' and prev_char == '[':
                output_text.pop()
                output_text.append(' '*indent_level)
                output_text.append(char)
            else:
                output_text.append('\n')
                output_text.append(' '*indent_level)
                output_text.append(char)

        elif char in (',',):
            output_text.append(char)
            output_text.append('\n')
            output_text.append(' '*indent_level)

        elif char == ' ':
            pass

        elif char in ('=',):
            output_text.append(' ')
            output_text.append(char)
            output_text.append(' ')

        else:
            output_text.append(char)

        prev_char = char

    return ''.join(output_text)
