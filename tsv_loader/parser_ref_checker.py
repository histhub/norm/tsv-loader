from tsv_loader.load_plan_visitor import LoadPlanVisitor


class ParserReferenceChecker(LoadPlanVisitor):
    """Visitor to check that the parsers referenced in a load plan file are
    actually provided in the parsers dict.
    """

    def __init__(self, parsers_dict):
        self.errors = []
        self._parsers_dict = parsers_dict
        self._current_file = None

    def _previsit_file_spec(self, file_spec):
        self._current_file = file_spec.filename

    def _previsit_parse_target_rule(self, parse_target_rule):
        if parse_target_rule.parser_name not in self._parsers_dict:
            self.errors.append("file '%s': unknown parser '%s'" % (
                self._current_file, parse_target_rule.parser_name))
