# TSV-Loader metadata

__description__ = ("A tool that provides an easy way to load data from a set "
                   "of TSV files into an EAV database managed using eavlib.")

__author__ = "SSRQ-SDS-FDS Law Sources Foundation of the Swiss Lawyers Society"
__copyright__ = ("Copyright 2019-2021 %s" % __author__)
__license__ = "GPLv3"
__maintainer__ = __author__
# Versions as per PEP 440 (https://www.python.org/dev/peps/pep-0440/)
__version_info__ = (1, 0)
__version__ = "1.0"


class LoadError(Exception):
    pass


class LoadPlanError(Exception):
    pass
