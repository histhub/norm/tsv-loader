import datetime
import functools
import logging
import os

from eavlib.core.data_manager import DataManager
from eavlib.core.id_manager import SequenceIDManager

from tsv_loader import (LoadError, LoadPlanError)
from tsv_loader.tsv_file import TsvFile
from tsv_loader.load_plan_reprs import pprint
from tsv_loader.load_plan_parser import LoadPlanParser
from tsv_loader.parser_ref_checker import ParserReferenceChecker
from tsv_loader.column_ref_checker import ColumnReferenceChecker
from tsv_loader.line_rule_eav_builder import LineRuleEavBuilder
from tsv_loader.eav_node_writer import EavNodeWriter

LOG = logging.getLogger(__name__)


def timed(logger):
    def decorator(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            start = datetime.datetime.now()
            res = func(*args, **kwargs)
            delta = (datetime.datetime.now() - start).total_seconds()

            logger.debug("TIME = %2dh %02d' %06.3f\"" % (
                delta // 3600, delta // 60 % 60, delta % 60))

            return res
        return wrapped
    return decorator


class TsvLoader:
    """Load information from TSV files into an EAV repository.

    A connection to an already existing EAV repository is needed (with a model
    already loaded) and a load plan file specifying the files to be loaded and
    how their columns map to the model present in the EAV repository.

    Raises an exception when a condition appears that prevents the normal
    continuation, always trying to log as many issues as possible before
    giving up.

    :raises LoadPlanError, LoadError, RuntimeError
    """

    @timed(LOG)
    def __init__(self, graph, load_spec_file, parsers_dict={}, **kwargs):
        """Start the TSV loader.

        :param graph: a graph object to an already existing EAV repository
                      (with a model already loaded)
        :type graph: eavlib.graph.Graph Parameters:
        :param load_spec_file: a file specifying the files to be loaded and how
                               their columns map to the given model
        :param parsers_dict: a dictionary containing parsers, keyed
                             by the names they are referred to inside the plan
                             file. Parses are objects with a parse method that
                             gets free text as input and returns an EAV bag
                             instance.
        """
        self._config = {
            "max_wrong_width": int(kwargs.get("max_wrong_width", 50)),
            "batch_size": int(kwargs.get("batch_size", 10000)),
            "dry_run": bool(kwargs.get("dry_run", False)),
            "dbid_attr": str(kwargs.get("dbid_attr", "id")),
            }

        id_generator = SequenceIDManager(graph.connection, "object_id_seq")
        data_manager = DataManager(graph, id_generator)

        tsv_load_spec = self._parse_load_spec_file(load_spec_file)
        self._check_load_spec_integrity(tsv_load_spec, parsers_dict)

        eav_node_builder = LineRuleEavBuilder(
            id_generator, parsers_dict, self._config["dbid_attr"])

        with EavNodeWriter(
                data_manager, self._config["batch_size"]) as eav_node_writer:
            for file_spec in tsv_load_spec.file_specs:
                tsv_file = self._tsv_file(
                    tsv_load_spec.directory,
                    file_spec.filename, tsv_load_spec.params)
                LOG.info("Processing data file '%s'", tsv_file.full_path)
                eav_node_builder.set_current_file(file_spec.filename)
                for line in tsv_file:
                    LOG.debug("Processing line %s", line)
                    eav_node_builder.set_current_line(line)
                    for rule in file_spec.line_rules:
                        rule.traverse_with(eav_node_builder)
                        if self._config["dry_run"]:
                            continue
                        eav_node_writer.save(eav_node_builder.result)

        eav_node_builder.report_reference_usage(LOG)
        eav_node_builder.report_build_errors(LOG)

        if eav_node_builder.has_errors():
            raise LoadError("There were errors during the load process.")

    def _parse_load_spec_file(self, load_spec_file):
        """Parse the plan file and return the parsed representation."""
        LOG.info("Processing load plan '%s'", load_spec_file)
        try:
            with open(load_spec_file, mode="r", encoding="utf-8") as plan_file:
                file_content = plan_file.read()
        except Exception as e:
            LOG.error(
                "Cannot read load spec file '%s' (%s)", load_spec_file,
                e.strerror if isinstance(e, IOError) else e)
            raise

        try:
            parsed_repr = LoadPlanParser().parse(file_content)
            parsed_repr.directory = os.path.dirname(load_spec_file)
            for line in pprint(parsed_repr, 2).split("\n"):
                LOG.debug("LOAD SPEC     %s", line)
            return parsed_repr
        except Exception as e:
            raise LoadPlanError("Error in load plan file! %s", e) from e

    def _check_load_spec_integrity(self, tsv_load_spec, parsers_dict):
        """Carries out the following checks:
        - parsers mentioned in the load spec are present in the parsers dict,
        - the TSV files have a consistent number of columns, and
        - column numbers used in the load spec are not out of bounds.
        """
        found_errors = []

        used_parsers_checker = ParserReferenceChecker(parsers_dict)
        tsv_load_spec.traverse_with(used_parsers_checker)
        found_errors.extend(used_parsers_checker.errors)

        for file_spec in tsv_load_spec.file_specs:
            try:
                tsv_file = self._tsv_file(
                    tsv_load_spec.directory, file_spec.filename,
                    tsv_load_spec.params)
                found_errors.extend(tsv_file.check_columns(
                    report_max=self._config["max_wrong_width"]))
                if tsv_file.is_empty:
                    LOG.warning("File '%s' is empty" % (tsv_file.filename))
                if tsv_file.num_cols:
                    column_reference_checker = ColumnReferenceChecker(
                        tsv_file.num_cols)
                    file_spec.traverse_with(column_reference_checker)
                    found_errors.extend(column_reference_checker.errors)
            except Exception as e:
                found_errors.append(str(e))

        if found_errors:
            for msg in found_errors:
                LOG.error(msg)
            raise LoadPlanError("Errors found in load specification!")

    def _tsv_file(self, directory, filename, params):
        kwargs = {}
        if params:
            kwargs["skip_header"] = params.skip_header
            kwargs["drop_quotes"] = params.drop_quotes
        return TsvFile(directory, filename, **kwargs)
