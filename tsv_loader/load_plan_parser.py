#!/usr/bin/env python

from collections import namedtuple

from tsv_loader import LoadPlanError
from tsv_loader.load_plan_reprs import (
    TsvLoadSpec, LoadParams, FileSpec, BuildingRule, LinkingRule,
    NormalizingRule, Condition, CachedParentRule, InstanceRule, AttributeRule,
    BuildTargetRule, CachedTargetRule, DatabaseTargetRule, ParseTargetRule,
    CachedChildRule)
from tsv_loader.load_plan_lexer import LoadPlanLexer

ConfigPair = namedtuple('ConfigPair', ('key', 'value'))


def _error_unexpected(line, found):
    raise LoadPlanError("line %u: unexpected '%s'" % (line, found[0]))


def _error(line, msg):
    raise LoadPlanError("line %u: %s" % (line, msg))


def check_front_token(token_list, token_type, token_value=None):
    """Checks that the front token of the given list has the given type (and
    value when specified).

    Returns a boolean value; aborts only in case of an empty token list.
    """
    if token_list:
        if token_value is None:
            # Check only the token type
            return token_list[0].type == token_type
        else:
            # Full check, type and token value
            return token_list[0].type == token_type \
                and token_list[0].value == token_value
    else:
        raise LoadPlanError('empty token list')


def consume_front_token(token_list, token_type, **kwargs):
    """
    Checks that the front token of the given list has the given type (and
    value when specified). In this case removes the token from the list
    and returns it.
    Otherwise aborts parsing with an expectation message (if provided).

    Keyword arguments might be:

    - token_value  for punctuation tokens, for example
    - expecting    human readable indication of what was expected to be
                   in the token list in case a different token is found.
    """
    token_value = kwargs.get('token_value', None)
    expecting = kwargs.get('expecting', token_value)

    if not token_list:
        raise LoadPlanError('empty token list')

    first_token = token_list[0]
    if check_front_token(token_list, token_type, token_value):
        del token_list[0]
        return first_token
    else:
        if check_front_token(token_list, 'EOF'):
            _error(first_token.line, 'unexpected EOF found')
        elif expecting is None:
            raise LoadPlanError('empty expecting specification')
        else:
            _error(first_token.line, "expected '{}', but found '{}'".format(
                expecting, first_token.value))


class LoadPlanParser:
    """Parser reading textual load plan descriptions and turning them into
    TsvLoadSpec instances that can be used by the EAV loader to load
    information from TSV or Excel files into an EAV schema.
    """

    def __init__(self):
        """Creates a parser suitable to parse load plans for a particular model
        and transform them into TsvLoadSpec instances.
        """
        self.plan_lexer = LoadPlanLexer()
        self.default_params = {
            'file_format': 'tsv',
            'skip_header': True,
            'drop_quotes': True,
            }

    def parse(self, load_plan_text):
        """Parses the content of a load plan file and returns a TsvLoadSpec."""
        return self._parse_load_spec(self.plan_lexer.analyse(load_plan_text))

    def _parse_load_spec(self, tokens):
        """
            <load_plan>  =  <load_params>?  <file_plan> +
        """
        if check_front_token(tokens, 'NAM', 'PARAMS'):
            parameters = self._parse_load_params(tokens)
        else:
            parameters = LoadParams(
                self.default_params['file_format'],
                self.default_params['skip_header'],
                self.default_params['drop_quotes'])

        file_specs = []
        while True:
            file_spec = self._parse_file_spec(tokens)
            file_specs.append(file_spec)
            if check_front_token(tokens, 'EOF'):
                return TsvLoadSpec(parameters, file_specs)

    def _parse_load_params(self, tokens):
        """
            <load_params>  =  'PARAMS'  '('  <parameter> +  ')'
        """
        params_dict = dict(self.default_params)
        consume_front_token(tokens, 'NAM', token_value='PARAMS')
        consume_front_token(tokens, 'PUN', token_value='(')

        while True:
            if check_front_token(tokens, 'PUN', ')'):
                break
            config = self._parse_parameter(tokens)
            params_dict[config.key] = config.value

        consume_front_token(tokens, 'PUN', token_value=')')
        return LoadParams(
            params_dict['file_format'],
            params_dict['skip_header'],
            params_dict['drop_quotes'])

    def _parse_parameter(self, tokens):
        """
            <parameter>  =  'format'  (  'tsv'  |  'excel'  )
                         |  'header'  (  'read' |  'skip'   )
                         |  'quotes'  (  'keep' |  'drop'   )
        """
        if check_front_token(tokens, 'NAM', 'format'):
            consume_front_token(tokens, 'NAM', token_value='format')
            file_format = consume_front_token(tokens, 'NAM', expecting='file format: tsv|excel')
            return ConfigPair('file_format', file_format.value)

        if check_front_token(tokens, 'NAM', 'header'):
            consume_front_token(tokens, 'NAM', token_value='header')
            header_action = consume_front_token(tokens, 'NAM', expecting='a header action: skip|read')
            return ConfigPair('skip_header', header_action.value == 'skip')

        if check_front_token(tokens, 'NAM', 'quotes'):
            consume_front_token(tokens, 'NAM', token_value='quotes')
            quotes_action = consume_front_token(tokens, 'NAM', expecting='a quotes action: keep|drop')
            return ConfigPair('drop_quotes', quotes_action.value == 'drop')

        _error_unexpected(tokens[0].line, tokens[0].value)

    def _parse_file_spec(self, tokens):
        """
            <file_plan>  =  'FROM'  'FILE'  '('  quoted_text  <action_plan> +  ')'
        """
        consume_front_token(tokens, 'NAM', token_value='FROM')
        consume_front_token(tokens, 'NAM', token_value='FILE')
        consume_front_token(tokens, 'PUN', token_value='(')
        filename = consume_front_token(tokens, 'TXT', expecting='quoted file name')

        action_plans = []
        while True:
            condition = None
            if check_front_token(tokens, 'NAM', 'CASE'):
                condition = self._parse_condition(tokens)

            if check_front_token(tokens, 'NAM', 'LOAD'):
                action_plan = self._parse_building_rule(tokens, condition)
            elif check_front_token(tokens, 'NAM', 'LINK'):
                action_plan = self._parse_linking_rule(tokens, condition)
            elif check_front_token(tokens, 'NAM', 'NORMALIZE'):
                action_plan = self._parse_normalizing_rule(tokens, condition)
            else:
                _error(tokens[0].line, "expecting keyword 'LOAD', 'LINK', "
                       "or 'NORMALIZE', but found '%s'" % tokens[0].value)

            action_plans.append(action_plan)
            if check_front_token(tokens, 'PUN', ')'):
                break

        consume_front_token(tokens, 'PUN', token_value=')')
        return FileSpec(filename.value, action_plans)

    def _parse_linking_rule(self, tokens, condition):
        """
            <linking_plan>  =  'LINK'  class_name  <cache_key>  <cache_target>
        """
        consume_front_token(tokens, 'NAM', token_value='LINK')
        class_token = consume_front_token(tokens, 'NAM', expecting='class name')
        cache_key = self._parse_cache_key(tokens)
        child_rule = CachedChildRule(class_token.value, cache_key)
        parent_rule = self._parse_cache_target(tokens)
        return LinkingRule(condition, parent_rule, child_rule)

    def _parse_building_rule(self, tokens, condition):
        """
            <building_rule>  =  'LOAD'  class_name  <cache_key>?  <build_plan>  <cache_target>?
        """
        consume_front_token(tokens, 'NAM', token_value='LOAD')
        class_token = consume_front_token(tokens, 'NAM', expecting='class name')

        cache_key = None
        if check_front_token(tokens, 'PUN', '['):
            cache_key = self._parse_cache_key(tokens)

        instance_rule = self._parse_instance_rule(tokens, class_token.value)
        instance_rule.cache_key = cache_key

        parent_rule = None
        if check_front_token(tokens, 'NAM', 'INTO'):
            parent_rule = self._parse_cache_target(tokens)

        return BuildingRule(condition, parent_rule, instance_rule)

    def _parse_normalizing_rule(self, tokens, condition):
        """
            <normalizing_rule>  =  'NORMALIZE'  number  'AS'  number
        """
        consume_front_token(tokens, 'NAM', token_value='NORMALIZE')
        variant = consume_front_token(tokens, 'NUM').value
        consume_front_token(tokens, 'NAM', token_value='AS')
        standard = consume_front_token(tokens, 'NUM').value
        return NormalizingRule(condition, None, variant, standard)

    def _parse_condition(self, tokens):
        """
            <condition>  =  'CASE'  number  'IS'  [ 'NOT' ]  quoted_text
                         |  'CASE'  number  'IS'  [ 'NOT' ]  'IN'  '['  quoted_text_list  ']'
        """
        values_list = []
        is_negated = False
        consume_front_token(tokens, 'NAM', token_value='CASE')
        column_token = consume_front_token(tokens, 'NUM')
        consume_front_token(tokens, 'NAM', token_value='IS')

        if check_front_token(tokens, 'NAM', 'NOT'):
            consume_front_token(tokens, 'NAM', token_value='NOT')
            is_negated = True

        if check_front_token(tokens, 'TXT'):
            text_token = consume_front_token(tokens, 'TXT')
            values_list.append(text_token.value)

        elif check_front_token(tokens, 'NAM', 'IN'):
            consume_front_token(tokens, 'NAM', token_value='IN')
            consume_front_token(tokens, 'PUN', token_value='[')
            while True:
                text_token = consume_front_token(
                    tokens, 'TXT', expecting='quoted value in condition')
                values_list.append(text_token.value)
                if check_front_token(tokens, 'PUN', ']'):
                    break

            consume_front_token(tokens, 'PUN', token_value=']')

        else:
            fmt = "expecting quoted text or keyword 'IN', but found '{}'"
            _error(tokens[0].line, fmt.format(tokens[0].value))

        return Condition(column_token.value, set(values_list), is_negated)

    def _parse_instance_rule(self, tokens, class_name):
        """
            <instance_rule>    =  '('  <build_step> +  ')'
            <build_step>       =  <attribute_rule>
                               |  <relation_rule>

            <attribute_rule>   =  attribute_name  '='  <attribute_value>
            <attribute_value>  =  number
                               |  quoted_text
                               |  '|'  number  '|'
                               |  '|'  quoted_text  '|'

            <relation_rule>    =  relation_name   '='  <related_spec>
                               |  relation_name   '='  [   <related_spec> +   ]
            <related_spec>     =  class_name   <instance_rule>
                               |  class_name   <cache_key>
                               |  class_name   <db_id>
                               |  class_name   <parse_id>
        """
        instance_rule = InstanceRule(class_name, None, [], [])
        consume_front_token(tokens, 'PUN', token_value='(')

        while True:
            if check_front_token(tokens, 'PUN', ')'):
                consume_front_token(tokens, 'PUN', token_value=')')
                break

            # attribute or relation name
            property_name = consume_front_token(
                tokens, 'NAM', expecting='attribute or relation name').value

            # optionality
            is_optional = False
            if check_front_token(tokens, 'PUN', '?'):
                is_optional = True
                consume_front_token(tokens, 'PUN', token_value='?')

            # equals sign
            consume_front_token(tokens, 'PUN', token_value='=')

            # attribute specification: column number
            if check_front_token(tokens, 'NUM'):
                col_number = consume_front_token(tokens, 'NUM').value
                instance_rule.attribute_rules.append(AttributeRule(
                    property_name, is_optional, False, col_number))

            # attribute specification: literal value
            elif check_front_token(tokens, 'TXT'):
                literal = consume_front_token(tokens, 'TXT').value
                instance_rule.attribute_rules.append(AttributeRule(
                    property_name, is_optional, False, literal))

            # attribute specification: normalized value
            elif check_front_token(tokens, 'PUN', '|'):
                value_spec = self._parse_normalized_value(tokens)
                instance_rule.attribute_rules.append(AttributeRule(
                    property_name, is_optional, True, value_spec))

            # 1-to-1 relation
            elif check_front_token(tokens, 'NAM'):
                relation_rule = self._parse_relation_spec(
                    tokens, property_name, is_optional)
                instance_rule.relation_rules.append(relation_rule)

            # 1-to-N relation
            elif check_front_token(tokens, 'PUN', '['):
                relation_rules = self._parse_list_of_relation_spec(
                    tokens, property_name, is_optional)
                instance_rule.relation_rules.extend(relation_rules)

            else:
                _error_unexpected(tokens[0].line, tokens[0].value)

        return instance_rule

    def _parse_list_of_relation_spec(self, tokens, relation_name, is_optional):
        """
            Parses the list in brackets. The '[' token has already been checked.

            <relation_rule>  =  relation_name   '='  [   <related_spec> +   ]
        """
        consume_front_token(tokens, 'PUN', token_value='[')
        relation_rules = []

        while True:
            if check_front_token(tokens, 'PUN', ']'):
                if len(relation_rules) == 0:
                    _error(tokens[0].line, 'empty list of related instances')
                else:
                    consume_front_token(tokens, 'PUN', token_value=']')
                    return relation_rules

            rel_rule = self._parse_relation_spec(
                tokens, relation_name, is_optional)
            relation_rules.append(rel_rule)

    def _parse_relation_spec(self, tokens, relation_name, is_optional):
        """
            <related_spec>     =  class_name   <instance_rule>
                               |  class_name   <cache_key>
                               |  class_name   <db_id>
                               |  class_name   <parse_id>
        """
        class_name = consume_front_token(
            tokens, 'NAM', expecting='relation target class name').value

        if check_front_token(tokens, 'PUN', '('):
            # relation = Entity( ... build plan definition
            target_rule = self._parse_instance_rule(tokens, class_name)
            return BuildTargetRule(relation_name, is_optional, target_rule)
        elif check_front_token(tokens, 'PUN', '['):
            # relation = Entity[3]
            return CachedTargetRule(relation_name, is_optional, class_name,
                                    self._parse_cache_key(tokens))
        elif check_front_token(tokens, 'PUN', '<'):
            # relation = Entity<3>
            return DatabaseTargetRule(relation_name, is_optional, class_name,
                                      self._parse_database_id(tokens))
        elif check_front_token(tokens, 'PUN', '{'):
            # relation = Entity{ 3 parsing definition ...
            col_number, parser_name, params = self._parse_parsing_spec(tokens)
            return ParseTargetRule(relation_name, is_optional, class_name,
                                   col_number, parser_name, params)
        else:
            _error_unexpected(tokens[0].line, tokens[0].value)

    def _parse_guarded_value(self, tokens, envelope, msg):
        """
            <db_id>       =  '<'  (  number |  text  )  '>'
            <cache_key>   =  '['  (  number |  text  )  ']'
        """
        consume_front_token(tokens, 'PUN', token_value=envelope[0])
        if check_front_token(tokens, 'NUM'):
            ref_token = consume_front_token(tokens, 'NUM', expecting=msg)
            ret_value = int(ref_token.value)
        else:
            ref_token = consume_front_token(tokens, 'TXT', expecting=msg)
            ret_value = str(ref_token.value)
        consume_front_token(tokens, 'PUN', token_value=envelope[1])
        return ret_value

    def _parse_normalized_value(self, tokens):
        """
            <norm_value>   =  '|'  (  number |  text  )  '|'
        """
        return self._parse_guarded_value(
            tokens, '||',
            'normalized value as column number or string literal')

    def _parse_cache_key(self, tokens):
        """
            <cache_key>   =  '['  (  number |  text  )  ']'
        """
        return self._parse_guarded_value(
            tokens, '[]',
            'column number or string literal for inter-file linking')

    def _parse_database_id(self, tokens):
        """
            <db_id>     =  '<'  (  number |  text  )  '>'
        """
        return self._parse_guarded_value(
            tokens, '<>',
            'column number or string literal for database ids')

    def _parse_parsing_spec(self, tokens):
        """
            <parsing_spec>  =  '{'  'PARSE'  number  'WITH'  quoted_text  [  <instance_rule>  ]  '}'
        """
        consume_front_token(tokens, 'PUN', token_value='{')
        consume_front_token(tokens, 'NAM', token_value='PARSE')
        column_token = consume_front_token(tokens, 'NUM', expecting='column number to be parsed')
        consume_front_token(tokens, 'NAM', token_value='WITH')
        text_token = consume_front_token(tokens, 'TXT', expecting='parser name in quotes')

        params_rule = None
        if check_front_token(tokens, 'PUN', '('):
            params_rule = self._parse_instance_rule(tokens, None)

        consume_front_token(tokens, 'PUN', token_value='}')
        return (column_token.value, text_token.value, params_rule)

    def _parse_cache_target(self, tokens):
        """
            <cache_target>  =  'INTO'  class_name  <cache_key>  '.'  relation_name
        """
        if check_front_token(tokens, 'PUN', ')'):
            return None

        elif check_front_token(tokens, 'NAM', 'INTO'):
            consume_front_token(tokens, 'NAM', token_value='INTO')
            class_token = consume_front_token(tokens, 'NAM')
            linking_ref = self._parse_cache_key(tokens)
            consume_front_token(tokens, 'PUN', token_value='.')
            relation_token = consume_front_token(
                tokens, 'NAM', expecting='relation name')

            return CachedParentRule(
                relation_token.value, class_token.value, linking_ref)
        else:
            _error_unexpected(tokens[0].line, tokens[0].value)
