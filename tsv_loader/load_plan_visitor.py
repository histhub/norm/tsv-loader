from tsv_loader.load_plan_reprs import (
    LoadPlanRepr, TsvLoadSpec, LoadParams, FileSpec, LineActionRule,
    BuildingRule, LinkingRule, NormalizingRule, Condition, CachedParentRule,
    InstanceRule, AttributeRule, RelationRule, BuildTargetRule,
    CachedTargetRule, DatabaseTargetRule, ParseTargetRule, CachedChildRule)


class LoadPlanVisitor:
    """Sort of "visitor" for adding the previsit and postvisit methods used when
    they are traversed to load plan representations.
    """

    def __init__(self):
        pass

    def _stop_traversal(self):
        raise LoadPlanRepr.StopTraversal

    def previsit(self, node):  # noqa: C901
        self._previsit_load_plan_repr(node)
        if isinstance(node, TsvLoadSpec):
            self._previsit_tsv_load_spec(node)
        elif isinstance(node, LoadParams):
            self._previsit_load_params(node)
        elif isinstance(node, FileSpec):
            self._previsit_file_spec(node)
        elif isinstance(node, LineActionRule):
            self._previsit_line_action_rule(node)
            if isinstance(node, BuildingRule):
                self._previsit_building_rule(node)
            elif isinstance(node, LinkingRule):
                self._previsit_linking_rule(node)
            elif isinstance(node, NormalizingRule):
                self._previsit_normalizing_rule(node)
        elif isinstance(node, Condition):
            self._previsit_condition(node)
        elif isinstance(node, CachedParentRule):
            self._previsit_cached_parent_rule(node)
        elif isinstance(node, InstanceRule):
            self._previsit_instance_rule(node)
        elif isinstance(node, AttributeRule):
            self._previsit_attribute_rule(node)
        elif isinstance(node, RelationRule):
            self._previsit_relation_rule(node)
            if isinstance(node, BuildTargetRule):
                self._previsit_build_target_rule(node)
            elif isinstance(node, CachedTargetRule):
                self._previsit_cached_target_rule(node)
            elif isinstance(node, DatabaseTargetRule):
                self._previsit_database_target_rule(node)
            elif isinstance(node, ParseTargetRule):
                self._previsit_parse_target_rule(node)
        elif isinstance(node, CachedChildRule):
            self._previsit_cached_child_rule(node)

    def postvisit(self, node):  # noqa: C901
        if isinstance(node, TsvLoadSpec):
            self._postvisit_tsv_load_spec(node)
        elif isinstance(node, LoadParams):
            self._postvisit_load_params(node)
        elif isinstance(node, FileSpec):
            self._postvisit_file_spec(node)
        elif isinstance(node, LineActionRule):
            if isinstance(node, BuildingRule):
                self._postvisit_building_rule(node)
            elif isinstance(node, LinkingRule):
                self._postvisit_linking_rule(node)
            elif isinstance(node, NormalizingRule):
                self._postvisit_normalizing_rule(node)
            self._postvisit_line_action_rule(node)
        elif isinstance(node, Condition):
            self._postvisit_condition(node)
        elif isinstance(node, CachedParentRule):
            self._postvisit_cached_parent_rule(node)
        elif isinstance(node, InstanceRule):
            self._postvisit_instance_rule(node)
        elif isinstance(node, AttributeRule):
            self._postvisit_attribute_rule(node)
        elif isinstance(node, RelationRule):
            if isinstance(node, BuildTargetRule):
                self._postvisit_build_target_rule(node)
            elif isinstance(node, CachedTargetRule):
                self._postvisit_cached_target_rule(node)
            elif isinstance(node, DatabaseTargetRule):
                self._postvisit_database_target_rule(node)
            elif isinstance(node, ParseTargetRule):
                self._postvisit_parse_target_rule(node)
            self._postvisit_relation_rule(node)
        elif isinstance(node, CachedChildRule):
            self._postvisit_cached_child_rule(node)
        self._postvisit_load_plan_repr(node)

    def _previsit_load_plan_repr(self, load_plan_repr):
        pass

    def _previsit_tsv_load_spec(self, tsv_load_spec):
        pass

    def _previsit_load_params(self, load_params):
        pass

    def _previsit_file_spec(self, file_spec):
        pass

    def _previsit_line_action_rule(self, line_action_rule):
        pass

    def _previsit_building_rule(self, building_rule):
        pass

    def _previsit_linking_rule(self, linking_rule):
        pass

    def _previsit_normalizing_rule(self, normalizing_rule):
        pass

    def _previsit_condition(self, condition):
        pass

    def _previsit_cached_parent_rule(self, cached_parent_rule):
        pass

    def _previsit_instance_rule(self, instance_rule):
        pass

    def _previsit_attribute_rule(self, attribute_rule):
        pass

    def _previsit_relation_rule(self, relation_rule):
        pass

    def _previsit_build_target_rule(self, build_target_rule):
        pass

    def _previsit_cached_target_rule(self, cached_target_rule):
        pass

    def _previsit_database_target_rule(self, database_target_rule):
        pass

    def _previsit_parse_target_rule(self, parse_target_rule):
        pass

    def _previsit_cached_child_rule(self, cached_child_rule):
        pass

    def _postvisit_load_plan_repr(self, load_plan_repr):
        pass

    def _postvisit_tsv_load_spec(self, tsv_load_spec):
        pass

    def _postvisit_load_params(self, load_params):
        pass

    def _postvisit_file_spec(self, file_spec):
        pass

    def _postvisit_line_action_rule(self, line_action_rule):
        pass

    def _postvisit_building_rule(self, building_rule):
        pass

    def _postvisit_linking_rule(self, linking_rule):
        pass

    def _postvisit_normalizing_rule(self, normalizing_rule):
        pass

    def _postvisit_condition(self, condition):
        pass

    def _postvisit_cached_parent_rule(self, cached_parent_rule):
        pass

    def _postvisit_instance_rule(self, instance_rule):
        pass

    def _postvisit_attribute_rule(self, attribute_rule):
        pass

    def _postvisit_relation_rule(self, relation_rule):
        pass

    def _postvisit_build_target_rule(self, build_target_rule):
        pass

    def _postvisit_cached_target_rule(self, cached_target_rule):
        pass

    def _postvisit_database_target_rule(self, database_target_rule):
        pass

    def _postvisit_parse_target_rule(self, parse_target_rule):
        pass

    def _postvisit_cached_child_rule(self, cached_child_rule):
        pass
