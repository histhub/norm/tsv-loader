import logging

from eavlib.core import ChangeBatch

LOG = logging.getLogger(__name__)


class EavNodeWriter:
    """Persists EAV nodes that are trees, that is their graph of related
    sub-objects doesn't have cycles. The batch size refers to the number of
    trees delivered to the save() method, not to nodes that are actually
    written.
    """
    DEFAULT_BATCH_SIZE = 1000

    def __init__(self, data_manager, batch_size):
        self._data_manager = data_manager
        try:
            self._batch_size = max(int(batch_size), 1)
        except Exception:
            self._batch_size = self.DEFAULT_BATCH_SIZE

        self._eav_model = data_manager.eav_model()
        self._seen_ids = set()
        self._reset_batch()

    def __enter__(self):
        return self

    def __exit__(self, ex_type, ex_value, traceback):
        self._flush_batch()
        return False

    def save(self, node):
        if node is not None:
            self._recursive_write(node)
            self._write_counter += 1
            if self._write_counter >= self._batch_size:
                self._flush_batch()
                self._reset_batch()

    def _recursive_write(self, node):
        node_id = node.eav_id
        if not node_id:
            raise RuntimeError("Node without object ID: %s" % (node))

        entity = self._eav_model.entity(node.entity_name)

        # Write object if not done before. This node or some subnodes
        # could have been already persisted, so a memory is needed in
        # order not to write them more than once.
        if node_id not in self._seen_ids:
            self._seen_ids.add(node_id)
            self._change_batch.write_object(node_id, entity.id)

        # Attributes and relations are written unconditionally since
        # they (normally) carry additional information in the case of
        # repeated nodes.
        for attr_name, attr_value in node.attributes.items():
            attr_id = entity.attribute(attr_name).id
            self._change_batch.write_attribute(node_id, attr_id, attr_value)

        for rel_name, nodes_list in node.relations.items():
            rel_id = entity.relation(rel_name).id
            for rel_node in nodes_list:
                self._change_batch.write_relation(
                    node_id, rel_id, rel_node.eav_id)
                self._recursive_write(rel_node)

    def _flush_batch(self):
        #LOG.debug("FLUSH with %d elements", self._write_counter)
        # TODO: catch the exception, dump useful information and re-raise
        self._data_manager.execute_batch(self._change_batch)
        self._data_manager.commit()

    def _reset_batch(self):
        self._change_batch = ChangeBatch()
        self._write_counter = 0
