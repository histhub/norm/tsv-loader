import re

from collections import namedtuple


class LoadPlanLexer:
    Token = namedtuple('Token',  ('type', 'line', 'value'))
    Action = namedtuple('Action', ('regex', 'token'))

    actions = (
        Action(
            re.compile(r"""^(\s*\d+)"""),
            lambda lin, g1: LoadPlanLexer.Token('NUM', lin, int(g1))),
        Action(
            re.compile(r"""^(\s*'[^']*')"""),
            lambda lin, g1: LoadPlanLexer.Token('TXT', lin, g1.strip()[1:-1])),
        Action(
            re.compile(r"""^(\s*"[^"]*")"""),
            lambda lin, g1: LoadPlanLexer.Token('TXT', lin, g1.strip()[1:-1])),
        Action(
            re.compile(r"""^(\s*(?:\(|\)|=|\*|\[|\]|\{|\}|\||\?|\.|<|>))"""),
            lambda lin, g1: LoadPlanLexer.Token('PUN', lin, g1.strip())),
        Action(
            re.compile(r"""^(\s*\w+)"""),
            lambda lin, g1: LoadPlanLexer.Token('NAM', lin, g1.strip())),
        )

    white_space_regex = re.compile(r'\s+')
    comment_regex = re.compile(r'#[^\n]*')  # same comments as python

    def analyse(self, input_text):
        """Reads a sequence of

            ( WHITE_SPACE  ( COMMENT | TOKEN ) ) *  WHITE_SPACE ?

        reporting the tokens only.
        """

        lineno = 1
        tokens = []
        while True:
            if not input_text:
                break

            white_space_match = self.white_space_regex.match(input_text)
            if white_space_match:
                matched_input = white_space_match.group(0)
                num_new_lines = matched_input.count('\n')
                input_text = input_text[len(matched_input):]
                lineno += num_new_lines
                #print("{:>3} WS [{}], {}".format(lineno, matched_input, num_new_lines))

            if not input_text:
                break

            # either COMMENT
            comment_match = self.comment_regex.match(input_text)
            if comment_match:
                input_text = input_text[len(comment_match.group(0)):]
                #matched_input = comment_match.group(0)
                #print("{:>3} ## [{}]".format(lineno, matched_input))

            # or TOKEN
            else:
                action_match = None
                for action in self.actions:
                    action_match = action.regex.match(input_text)
                    if action_match:
                        input_text = input_text[len(action_match.group(1)):]
                        tokens.append(action.token(lineno, action_match.group(1)))
                        #print("{:>3} TK [{}]".format(lineno, action_match.group(1)))
                        break

                if action_match is None:
                    tokens.append(self.Token('ERR', lineno, input_text))
                    input_text = input_text[1:]

        tokens.append(LoadPlanLexer.Token('EOF', lineno, None))
        return tokens


if __name__ == '__main__':
    from sys import argv
    if len(argv) > 1:
        with open(argv[1], 'r') as input_file:
            content = input_file.read()
    else:
        content = """
            from file ( 'data/person.tsv'
               load Person[1] (
                  name? = 2
                  idol  = 'Pelé'
                  hates = 'Billy the Kid'
               )
            )
            from file ( 'data/place_mapping.tsv'
               link Place[2]
               into Person[1].locations
            )
        """

    separator = 90 * '-'

    print(separator)
    print(content)
    print(separator)

    lexer = LoadPlanLexer()
    tokens = lexer.analyse(content)
    for token in tokens:
        print(token)
