import re

from functools import wraps


class RegexBagBuilder:
    r"""Parses some free text and builds an EAV bag.

    A regex EAV bag builder returns an EAV Bag each time its "parse"
    method is called with some free text.

    It tries a sequence of regular expressions until it a matching
    one is found. The "building" function associated to that regex
    is invoked with the match and a default instance as parameters.
    The building function takes information from the match object
    and moves it to the appropriate attributes of the EAV bag being
    built.

    A list of building functions has to be provided at construction.
    Since the list is traversed sequentially, putting more frequent
    patterns first improves performance.

    A building function has two parameters: match and object being
    built. The "@using" decorator has to be used to tie the function
    with a particular regular expression and a factory function for
    building the (default) EAV bag used inside the function. As an
    example, for building dates:


    def bag_factory():
        return EavBag("Date")

    @RegexBagBuilder.using(bag_factory, r'^(\d{2})-(\d{2})-(\d{4})$')
    def my_date_builder(match, instance):
        instance.set_attribute('day',   match.group(1))
        instance.set_attribute('month', match.group(2))
        instance.set_attribute('year',  match.group(3))
    """

    def __init__(self, parsing_patterns):
        self.parsing_pattern_functions = parsing_patterns

    class NoMatchCondition(RuntimeError):
        """Exception to signal that the current pattern does not match so that
        the following one should be tried.
        """
        pass

    @staticmethod
    def using(*args, **kwargs):
        """Decorator used to tie a regular expression to a builder function."""
        factory, rest_args = args[0], args[1:]
        regex = re.compile(*rest_args, **kwargs)  # called only once by closure

        def decorate(func):
            @wraps(func)
            def builder(text):
                match = regex.match(text)
                if match:
                    instance = factory()
                    func(match, instance)
                    return instance
                else:
                    raise RegexBagBuilder.NoMatchCondition()
            return builder
        return decorate

    def parse(self, free_text):
        """Reads free text as input and returns an EAV bag."""
        if not free_text:
            raise RuntimeError('empty input')

        for parsing_pattern in self.parsing_pattern_functions:
            try:
                return parsing_pattern(free_text.strip())
            except RegexBagBuilder.NoMatchCondition:
                pass

        fmt = "unrecognized text '{}'"
        raise RuntimeError(fmt.format(free_text.strip()))
