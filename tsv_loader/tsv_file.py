import csv
import os

from collections import Counter


class TsvLine:
    """Holder of the fields of a line in a TSV file together with the line
    number (1 based) and the filename it was read from.
    """
    __slots__ = ("_fields", "line_num", "filename")

    def __init__(self, fields, line_num, filename, drop_quotes=False):
        def remove_quotes(text):
            if not text or len(text) < 2:
                return text
            elif text[0] == text[-1] and text[0] in ('"', "'"):
                return text[1:-1]
            else:
                return text

        self._fields = fields
        self.line_num = line_num
        self.filename = filename

        if drop_quotes:
            self._fields = [remove_quotes(v) for v in self._fields]

    def __getitem__(self, index):
        # column numbers starting at 1
        return self._fields[index - 1]

    def __iter__(self):
        return iter(self._fields)

    def __str__(self):
        return "file='{}' L{} |{}|".format(
            self.filename, self.line_num, "|".join(self._fields))


class TsvFile:
    """Class representing a TSV file, able to check the consistency of its
    columns and produce iterators over its lines.
    """

    def __init__(self, directory, filename, **kwargs):
        self.full_path = os.path.join(directory, filename)
        if not os.path.exists(self.full_path):
            raise FileNotFoundError("cannot access file '%s' (in dir '%s')" % (
                filename, directory))

        self.filename = filename
        self.skip_header = bool(kwargs.get("skip_header", True))
        self.drop_quotes = bool(kwargs.get("drop_quotes", True))
        self.num_lines = 0
        self.num_cols = 0
        self.is_empty = True

    @staticmethod
    def __iter_csv(full_path, encoding="utf-8", dialect="excel-tab"):
        with open(full_path, newline="", encoding=encoding) as fh:
            yield from csv.reader(fh, dialect=dialect)

    def __iter__(self):
        it = self.__iter_csv(self.full_path)
        if self.skip_header:
            next(it, None)

        for i, row in enumerate(it, start=(self.skip_header + 1)):
            yield TsvLine(row, i, self.filename, self.drop_quotes)

    def check_columns(self, report_max=50):
        """Check that the file has a consistent number of columns.

        :param report_max: Maximum number of errors to report. If more errors
        are found, only one mesage will be returned.
        :type report_max: int
        :returns: list -- a list of error messages for offending lines.
        """
        found_errors = []
        ncols = tuple(map(len, self.__iter_csv(self.full_path)))

        self.num_lines = len(ncols)
        self.is_empty = not bool(max(0, self.num_lines - self.skip_header))

        cols_counter = Counter(ncols)
        if len(cols_counter) == 1:
            self.num_cols = ncols[0]
            return found_errors

        # Note: It is assumed that the most frequent occurrence of column
        # numbers is the "correct" one.
        most_freq_ncols = cols_counter.most_common(1)[0][0]
        num_errors = sum(cols_counter.values()) - cols_counter[most_freq_ncols]

        if num_errors > report_max:
            found_errors.append(
                "File '%s': too many lines have wrong number of columns" % (
                    self.filename))
        else:
            wrong_lines = (
                ln for ln, ncols in enumerate(ncols, start=1)
                if ncols != most_freq_ncols)
            for line in wrong_lines:
                # Note: Too many columns is also treated as an error because
                #       an excess of columns could be a sign of the presence of
                #       field delimiters inside the data.

                found_errors.append(
                    "File '%s', line %d: %s columns than expected" % (
                        self.filename, line,
                        "fewer" if ncols[line-1] < most_freq_ncols else "more"
                    ))

        return found_errors
