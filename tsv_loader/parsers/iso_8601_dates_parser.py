from eavlib.core.eav_bag import EavBag
from tsv_loader.regex_bag_builder import RegexBagBuilder


# Pattern for parsing text strings as ISO 8601 dates.

#   00
#   871
#   0690
#   268-09
#   0999-12
#   1795-10
#   1744-05
#   1974-05-06
#
@RegexBagBuilder.using(
    lambda: EavBag('Date'),
    r'^(\d{2,4})(-(\d{2}))?(-(\d{2}))?$'
)   # 1        2 3        4 5
def __9999_99_99__(match, instance):

    instance.set_attribute('calendar', 'gregorian')
    instance.set_attribute('type',     'certain')
    instance.set_attribute('size',     '1')
    instance.set_attribute('mode',     'after')

    instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('unit', 'year')

    instance.set_attribute('month', '01')
    if match.group(2):
        instance.set_attribute('month', "{:>02}".format(match.group(3)))
        instance.set_attribute('unit', 'month')

    instance.set_attribute('day', '01')
    if match.group(4):
        instance.set_attribute('day', "{:>02}".format(match.group(5)))
        instance.set_attribute('unit', 'day')


def iso_8601_dates_parser():
    """Returns a parser for dates in ISO 8601 format."""
    return RegexBagBuilder((__9999_99_99__,))


if __name__ == '__main__':
    try:
        from sys import argv
        free_text = argv[1]
    except IndexError:
        free_text = "1765-09-24"

    try:
        print(free_text)
        print(30*'=')
        date_parser = iso_8601_dates_parser()
        print(date_parser.parse(free_text))
    except Exception as e:
        from traceback import print_exc
        print_exc()
        print(e)
