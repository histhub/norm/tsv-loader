import re

from eavlib.core.eav_bag import EavBag


class HistHubForenamesParser:
    # Forename components are separated by whitespace: "Jean-Jacques André"
    FORENAME_PARTS_SEPARATOR = ' '

    def __init__(self):
        self._whitespace_regex = re.compile(r'\s+')

    def parse(self, free_text, params=None):
        """Reads free text as input and returns a list of EAV ForenamePart nodes.
        A 'lang' parameter containing a valid language Thesaurus-Concept is
        expected in the params dict.
        """
        if not free_text:
            raise RuntimeError('empty input')

        # Collapse whitespace sequences into a single whitespace character.
        normalized_input = self._whitespace_regex.sub(
            self.FORENAME_PARTS_SEPARATOR,
            free_text.strip()
        )

        if 'lang' not in params:
            raise RuntimeError("missing 'lang' parsing parameter")

        order_number = 0
        forenames_list = []

        for name in normalized_input.split(self.FORENAME_PARTS_SEPARATOR):
            order_number += 1

            appellation = EavBag('Appellation')
            appellation.set_attribute('text', name.strip())
            if 'lang' in params:
                appellation.set_attribute('lang', params['lang'])

            name_part = EavBag('ForenamePart')
            name_part.set_attribute('order', order_number)
            name_part.add_related('forename', appellation)
            forenames_list.append(name_part)

        return forenames_list


def histhub_forenames_parser():
    """Returns the histHub parser for SSRQ durations."""
    return HistHubForenamesParser()


if __name__ == '__main__':
    separator = 50 * '-'
    fp_parser = histhub_forenames_parser()

    from sys import stdin
    for line in stdin:
        try:
            free_text = line[0:-1]
            print("INPUT '{}'".format(free_text))
            print(separator)
            for item in fp_parser.parse(free_text):
                print(item)
                print(separator)
            print("\n")
        except Exception as e:
            #from traceback import print_exc
            #print_exc()
            print("ERROR: {}".format(e))
