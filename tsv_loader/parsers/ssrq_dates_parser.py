# TODO: there is no validity check for the parsed date: 34.78.1765 yields day=34, month=78, ...

import re
from datetime import (date, timedelta)
from calendar import monthrange

from eavlib.core.eav_bag import EavBag
from tsv_loader.regex_bag_builder import RegexBagBuilder


MONTH_NUMBER = {
    'Januar':     1,  'Jan.':  1,
    'Februar':    2,  'Feb.':  2,
    'März':       3,  'Mär.':  3,
    'April':      4,  'Apr.':  4,
    'Mai':        5,  'Mai.':  5,
    'Juni':       6,  'Jun.':  6,
    'Juli':       7,  'Jul.':  7,
    'August':     8,  'Aug.':  8,
    'September':  9,  'Sep.':  9,
    'Oktober':   10,  'Okt.': 10,
    'November':  11,  'Nov.': 11,
    'Dezember':  12,  'Dez.': 12,
    }


# Function producing a default Date instances, later passed to the building
# functions to populate it with matched information.
def default_date_instance():
    bag = EavBag('Date')

    bag.set_attribute('calendar', 'gregorian')
    bag.set_attribute('year',     '01')
    bag.set_attribute('month',    '01')
    bag.set_attribute('day',      '01')
    bag.set_attribute('type',     'certain')
    bag.set_attribute('size',     '1')
    bag.set_attribute('unit',     'year')
    bag.set_attribute('mode',     'after')

    return bag

# Patterns for parsing free text strings into histHub norm ontology
# DateIndications instances. They are defined most frequent first.

#   781
#   1249
#   5 a.C.
#   81 a.C.
#   249 a.C.
#   1249 a.C.
#   5 p.C.
#   81 p.C.
#   249 p.C.
#   1249 p.C.
#   morte en 987
#   mort en 1706
@RegexBagBuilder.using(default_date_instance, r'^(?:morte? en )?(\d{1,4})( [ap]\.C.)?$')
def __9opt999__(match, instance):
    if match.group(2) and match.group(2) == ' a.C.':
        instance.set_attribute('year', "-{:>04}".format(match.group(1)))
    else:
        instance.set_attribute('year', "{:>04}".format(match.group(1)))


#   05.1643
#   26.12.1194
#   19.06.1805?
@RegexBagBuilder.using(default_date_instance, r'^(\d{1,2}\.)?(\d{1,2})\.(\d{3,4})(\?)?$')
def __9opt9opt_99_9999__(match, instance):
    if match.group(1) is None:
        instance.set_attribute('unit', 'month')
    else:
        instance.set_attribute('day', "{:>02}".format(match.group(1)[:-1]))
        instance.set_attribute('unit', 'day')
    instance.set_attribute('month', "{:>02}".format(match.group(2)))
    instance.set_attribute('year', "{:>04}".format(match.group(3)))
    instance.set_attribute('type', 'expected' if match.group(4) else 'certain')


#   um 860
#   um 1400
@RegexBagBuilder.using(default_date_instance, r'^um (\d{1,4})$')
def __um_9opt999__(match, instance):
    instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('type', 'expected')
    instance.set_attribute('size', '10')
    instance.set_attribute('unit', 'year')
    instance.set_attribute('mode', 'around')


#   vor 1230
#   vor 1453 (?)
#   bis 1230
#   bis 1453 (?)
#   avant 1730
#   avant 1453 (?)
#   av. 1730
#   av. 1453 (?)
#   wohl vor 1230
#   bien avant 1230
#   ante 1730
#   ante 1453 (?)
#   ante 1453!
#   ante 853!
@RegexBagBuilder.using(default_date_instance, r'^(wohl |bien )?(?:vor|bis|avant|ante|av\.) (\d{1,4})(!|\?| \(\?\))?$')
def __vor_9999__(match, instance):
    instance.set_attribute('year', "{:>04}".format(match.group(2)))
    instance.set_attribute('type', 'expected')
    instance.set_attribute('size', '10' if match.group(1) else '50')
    instance.set_attribute('unit', 'year')
    instance.set_attribute('mode', 'before')


#   vor 02.09.1493
#   kurz vor 02.09.1493
#   vor dem 02.09.1493
#   kurz vor dem 02.09.1493
@RegexBagBuilder.using(default_date_instance, r'^(kurz )?(vor|av.) (?:dem )?(\d{1,2})\.(\d{1,2})\.(\d{3,4})$')
def __vor_99_99_9999__(match, instance):
    instance.set_attribute('day',   "{:>02}".format(match.group(3)))
    instance.set_attribute('month', "{:>02}".format(match.group(4)))
    instance.set_attribute('year',  "{:>04}".format(match.group(5)))
    instance.set_attribute('type',  'expected')
    instance.set_attribute('size',  '3' if match.group(1) else '10')
    instance.set_attribute('unit',  'day')
    instance.set_attribute('mode',  'before')


#   15.Jh.
#   15. Jh.
#   wohl 19. Jh.
#   19. Jh.?
@RegexBagBuilder.using(default_date_instance, r'^(wohl )?(\d{1,2})\. *Jh\.?(\?)?$')
def __99_Jh__(match, instance):
    instance.set_attribute('year', "{:<02}01".format(int(match.group(2))-1))
    instance.set_attribute('size', '100')
    instance.set_attribute('unit', 'year')
    if match.group(1) is None and match.group(3) is None:
        instance.set_attribute('type', 'certain')
    else:
        instance.set_attribute('type', 'expected')


#   ab 1610
#   ab 12.08.1610
#   nach 1613
#   kurz nach 1613
#   post1615
#   post 1615
#   post  1615
#   post 1615!
#   post 16 a.C.
#   post 16 p.C.
@RegexBagBuilder.using(default_date_instance, r'^(kurz )?(?:ap\.|après|nach|ab|post) *((\d{2})\.(\d{2})\.)?(\d{1,4})(!)?( [ap]\.C\.)?$')
def __nach_9999__(match, instance):
    if match.group(7) and match.group(7) == ' a.C.':
        instance.set_attribute('year', "-{:>04}".format(match.group(5)))
    else:
        instance.set_attribute('year', "{:>04}".format(match.group(5)))
    if match.group(4):
        instance.set_attribute('month', match.group(4))
    if match.group(3):
        instance.set_attribute('day',   match.group(3))
    instance.set_attribute('type', 'expected')
    instance.set_attribute('size', '50' if match.group(1) is None else '10')
    instance.set_attribute('unit', 'year')
    instance.set_attribute('mode', 'after')


#   ca.765
#   ca.1432
#   ca. 1581
#   ca.  1876
#   ca.765!
#   ca.1432!
#   ca. 1581!
#   ca.  1876!
#   ca. 1 a.C.
#   ca. 15 a.C.
#   ca. 135 a.C.
#   ca. 45 p.C.
@RegexBagBuilder.using(default_date_instance, r'^ca\. *(\d{1,4})!?( [ap]\.C\.)?$')
def __ca_9999__(match, instance):
    if match.group(2) and match.group(2) == ' a.C.':
        instance.set_attribute('year', "-{:>04}".format(match.group(1)))
    else:
        instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('type', 'expected')
    instance.set_attribute('size', '10')
    instance.set_attribute('unit', 'year')
    instance.set_attribute('mode', 'around')


#   vor 10.1738
@RegexBagBuilder.using(default_date_instance, r'^(vor|avant|av\.) (\d{1,2})\.(\d{1,4})$')
def __vor_99_9999__(match, instance):
    instance.set_attribute('month', "{:>02}".format(match.group(2)))
    instance.set_attribute('year', "{:>04}".format(match.group(3)))
    instance.set_attribute('type', 'expected')
    instance.set_attribute('size', '3')
    instance.set_attribute('unit', 'month')
    instance.set_attribute('mode', 'before')


#   603/4
#   1503/08
#   1511/1515
#   1549-1566
#   1549–1566     "En Dash" (U+2013) and
#   1549—1566     "em Dash" (U+2014) also taken into account.
#   um 1430/40
#   ca. 1628/29
#   ca. 1655/1656
#   1503/08!
#   1511/1515!
#   10/15 p.C.
#   15/10 a.C.
@RegexBagBuilder.using(default_date_instance, r'^(um|ca\.?)? *(\d{2,4})[—/–-](\d{1,4})(!)?( [ap]\.C\.)?$')
def __9999_999opt9opt__(match, instance):
    if match.group(5) and match.group(5) == ' a.C.':
        start_year = -int(match.group(2))
        end_year = -int(match.group(2)[0:-len(match.group(3))] + match.group(3))
    else:
        start_year = int(match.group(2))
        end_year = int(match.group(2)[0:-len(match.group(3))] + match.group(3))
    difference = end_year - start_year

    if match.group(1) is None and match.group(3) is None:
        instance.set_attribute('type', 'certain')
    else:
        instance.set_attribute('type', 'expected')
        if match.group(1) == 'um':
            start_year -= 10
            difference += 20

    if match.group(5) and match.group(5) == ' a.C.':
        instance.set_attribute('year', "-{:>04}".format(-start_year))
    else:
        instance.set_attribute('year', "{:>04}".format(start_year))
    instance.set_attribute('size', str(1 + difference))
    instance.set_attribute('unit', 'year')
    instance.set_attribute('mode', 'after')


#   déjà mort en 1763
#   Déjà mort en 1704
#   Déjà morte en 1712
@RegexBagBuilder.using(default_date_instance, r'^déjà morte? en (\d{1,4})$', re.IGNORECASE)
def __deja_mort_en_9999__(match, instance):
    instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('type', 'expected')
    instance.set_attribute('size', '10')
    instance.set_attribute('unit', 'year')
    instance.set_attribute('mode', 'before')


#   wohl 1501
#   angeblich 1664
#   vermutlich 1527
@RegexBagBuilder.using(default_date_instance, r'^(?:angeblich|vermutlich|wohl) (\d{1,4})$')
def __prob_9999__(match, instance):
    instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('type', 'expected')


#   15?86
@RegexBagBuilder.using(default_date_instance, r'^(\d{1,2})\?(\d{2})$')
def __99quest99__(match, instance):
    year = 100 * int(match.group(1)) + int(match.group(2))
    instance.set_attribute('year', "{:>04}".format(year))
    instance.set_attribute('type', 'expected')


#   nach 17.10.1528
#   nach dem 17.10.1528
#   kurz nach 17.10.1528
#   kurz nach dem 17.10.1528
@RegexBagBuilder.using(default_date_instance, r'^(kurz )?nach (?:dem )?(\d{2})\.(\d{2})\.(\d{4})$')
def __nach_99_99_9999__(match, instance):
    instance.set_attribute('day',   match.group(2))
    instance.set_attribute('month', match.group(3))
    instance.set_attribute('year',  match.group(4))
    instance.set_attribute('type',  'expected')
    instance.set_attribute('size',  '3' if match.group(1) else '10')
    instance.set_attribute('unit',  'day')
    instance.set_attribute('mode',  'after')


#   781?
#   781!
#   1249?
#   1249!
@RegexBagBuilder.using(default_date_instance, r'^(\d{1,4})(\?|!)$')
def __9opt999quest__(match, instance):
    instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('type', 'expected')


#   1666 (?)
@RegexBagBuilder.using(default_date_instance, r'^(\d{1,4}) \(\?\)$')
def __9999_quest__(match, instance):
    instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('type', 'expected')


#   zwischen 1401 und 1425
#   nach 1401 vor 1425
#   entre 1401 et 1425
#   zwischen 17.03.1401 und 1425
#   zwischen 1401 und 17.11.1425
#   zwischen 17.03.1401 und 12.09.1425
@RegexBagBuilder.using(default_date_instance, r'^(zwischen|entre|nach) ((\d{2})\.(\d{2})\.)?(\d{1,4}) (und|et|vor) ((\d{2})\.(\d{2})\.)?(\d{1,4})$')
#                                                1                23        4          5         6        78        9          10
#                                                                 12        3          4                  56        7          8
def __zwischen_9999_und_9999__(match, instance):
    if match.group(2) is None and match.group(7) is None:
        diff = int(match.group(10)) - int(match.group(5))
        instance.set_attribute('year', "{:>04}".format(match.group(5)))
        instance.set_attribute('type', 'certain')
        instance.set_attribute('unit',  'year')
        instance.set_attribute('size', str(1 + diff))
    else:
        start_dd = 1 if match.group(3) is None else int(match.group(3))
        start_mm = 1 if match.group(4) is None else int(match.group(4))
        start_yy = int(match.group(5))

        end_dd = 31 if match.group(8) is None else int(match.group(8))
        end_mm = 12 if match.group(9) is None else int(match.group(9))
        end_yy = int(match.group(10))

        diff = (date(end_yy, end_mm, end_dd) - date(start_yy, start_mm, start_dd)).days
        instance.set_attribute('year',  "{:>04}".format(start_yy))
        instance.set_attribute('month', "{:>02}".format(start_mm))
        instance.set_attribute('day',   "{:>02}".format(start_dd))
        instance.set_attribute('type', 'certain')
        instance.set_attribute('unit',  'day')
        instance.set_attribute('size', str(1 + diff))


#   récemment mort en 1783
@RegexBagBuilder.using(default_date_instance, r'^récemment morte? en (\d{1,4})$')
def __recement_mort_9999__(match, instance):
    instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('type', 'expected')
    instance.set_attribute('size', '5')
    instance.set_attribute('unit', 'year')
    instance.set_attribute('mode', 'before')


#   cité en 1686
@RegexBagBuilder.using(default_date_instance, r'^citée? en (\d{1,4})$')
def __cite_en_9opt999__(match, instance):
    instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('type', 'expected')


#   1380er Jahre
@RegexBagBuilder.using(default_date_instance, r'^(\d{1,4})er Jahre$')
def __9999er_jahre__(match, instance):
    instance.set_attribute('year', "{:>04}".format(match.group(1)))
    instance.set_attribute('size', '10')
    instance.set_attribute('unit', 'year')
    instance.set_attribute('mode', 'after')


#   Anfang 13. Jh.
#   anfangs 13. Jh.
#   Anfang 15. Jh.?
#   Ende 13. Jh.
#   ende 13. Jh.
#   Mitte 12. Jh.
@RegexBagBuilder.using(default_date_instance, r'^(anfangs|Anfang|Mitte|mitte|Ende|ende) (\d{1,2})\. Jh\.(\?)?$')
def __anfang_99_Jh__(match, instance):
    if match.group(1).endswith('nde'):
        instance.set_attribute('year', "{:<02}01".format(int(match.group(2))))
        instance.set_attribute('mode', 'before')
    elif match.group(1).endswith('itte'):
        instance.set_attribute('year', "{:<02}50".format(int(match.group(2))-1))
        instance.set_attribute('mode', 'around')
    else:
        instance.set_attribute('year', "{:<02}01".format(int(match.group(2))-1))
        instance.set_attribute('mode', 'after')
    instance.set_attribute('size', '25')
    instance.set_attribute('unit', 'year')
    instance.set_attribute('type', 'expected')


#   1. Hälfte 15. Jh.
#   2. Hälfte 18. Jh.
#   1. Hälfte 15. Jh.?
@RegexBagBuilder.using(default_date_instance, r'^(1|2)\. H(älfte|.) (\d{1,2})\. Jh\.(\?)?$')
def __9_haelfte_99_Jh__(match, instance):
    if match.group(1) == '1':
        instance.set_attribute('year', "{:<02}01".format(int(match.group(3))-1))
        instance.set_attribute('mode', 'after')
    else:
        instance.set_attribute('year', "{:<02}01".format(int(match.group(3))))
        instance.set_attribute('mode', 'before')
    instance.set_attribute('size', '50')
    instance.set_attribute('unit', 'year')
    instance.set_attribute('type', 'expected')


#   17. oder 18. Jh.
#   8./9. Jh.
#   18./19. Jh.
#   8./9.Jh.
#   18./19.Jh.
#   8/9.Jh.
@RegexBagBuilder.using(default_date_instance, r'^(\d{1,2})\.?(?:/| oder )(\d{1,2})\. *Jh\.$')
def __99_oder_99_Jh__(match, instance):
    diff = 100 * (int(match.group(2)) - int(match.group(1)) + 1)
    instance.set_attribute('year', "{:<02}01".format(int(match.group(1))-1))
    instance.set_attribute('size', str(diff))
    instance.set_attribute('unit', 'year')
    instance.set_attribute('type', 'certain')


#   frühestens 1429
#   spätestens 1521
@RegexBagBuilder.using(default_date_instance, r'^(frühestens|spätestens) (\d{1,4})$')
def __frsp_9999__(match, instance):
    year = int(match.group(2))
    instance.set_attribute('type', 'expected')
    instance.set_attribute('size', '50')
    if match.group(1) == 'spätestens':
        instance.set_attribute('year',  "{:>04}".format(year + 1))
        instance.set_attribute('mode',  'before')
    else:
        instance.set_attribute('year',  "{:>04}".format(year))
        instance.set_attribute('mode',  'after')


#   Ende 598
#   Ende 1598
#   Mitte 529
#   Mitte 1529
#   anfangs 876
#   anfangs 1876
@RegexBagBuilder.using(default_date_instance, r'^(anfangs|Ende|Mitte|Herbst|Sommer) (\d{3,4})$')
def __anfangs_mitte_ende_9opt999__(match, instance):
    year = int(match.group(2))

    if match.group(1) == 'anfangs':
        instance.set_attribute('year',  "{:>04}".format(year))
        instance.set_attribute('month', '01')
        instance.set_attribute('day',   '01')
        instance.set_attribute('type',  'expected')
        instance.set_attribute('size',  '100')
        instance.set_attribute('unit',  'day')
        instance.set_attribute('mode',  'after')

    elif match.group(1) == 'Mitte':
        instance.set_attribute('year',  "{:>04}".format(year))
        instance.set_attribute('month', '07')
        instance.set_attribute('day',   '01')
        instance.set_attribute('type',  'expected')
        instance.set_attribute('size',  '50')
        instance.set_attribute('unit',  'day')
        instance.set_attribute('mode',  'around')

    elif match.group(1) == 'Ende':
        instance.set_attribute('year',  "{:>04}".format(year + 1))
        instance.set_attribute('month', '01')
        instance.set_attribute('day',   '01')
        instance.set_attribute('type',  'expected')
        instance.set_attribute('size',  '100')
        instance.set_attribute('unit',  'day')
        instance.set_attribute('mode',  'before')

    elif match.group(1) == 'Sommer':
        instance.set_attribute('year',  "{:>04}".format(year))
        instance.set_attribute('month', '06')
        instance.set_attribute('day',   '21')
        instance.set_attribute('type',  'certain')
        instance.set_attribute('size',  '92')
        instance.set_attribute('unit',  'day')
        instance.set_attribute('mode',  'after')

    elif match.group(1) == 'Herbst':
        instance.set_attribute('year',  "{:>04}".format(year))
        instance.set_attribute('month', '09')
        instance.set_attribute('day',   '21')
        instance.set_attribute('type',  'certain')
        instance.set_attribute('size',  '92')
        instance.set_attribute('unit',  'day')
        instance.set_attribute('mode',  'after')


#   um 12.03.860
#   um 11.04.1400
@RegexBagBuilder.using(default_date_instance, r'^(ca.|um) (\d{2})\.(\d{2})\.(\d{1,4})$')
def __um_99_99_9opt999__(match, instance):
    instance.set_attribute('day',   "{:>02}".format(match.group(2)))
    instance.set_attribute('month', "{:>02}".format(match.group(3)))
    instance.set_attribute('year',  "{:>04}".format(match.group(4)))
    instance.set_attribute('type', 'expected')
    instance.set_attribute('size', '10')
    instance.set_attribute('unit', 'day')
    instance.set_attribute('mode', 'around')


#   894 ou 895
#   894 oder 895
#   1894 ou 1895
#   1894 oder 1895
@RegexBagBuilder.using(default_date_instance, r'^(\d{3,4}) (?:oder|ou) (\d{3,4})$')
def __9999_oder_9999__(match, instance):
    first_year = int(match.group(1))
    second_year = int(match.group(2))
    if second_year - first_year == 1:
        instance.set_attribute('year', "{:>04}".format(first_year))
        instance.set_attribute('type', 'certain')
        instance.set_attribute('size', '2')
        instance.set_attribute('unit', 'year')
        instance.set_attribute('mode', 'after')
    else:
        raise RegexBagBuilder.NoMatchCondition()


#   12./13.03.860
#   12./13.03.1860
#   01.09./20.11.1165
@RegexBagBuilder.using(default_date_instance, r'^(\d{2})\.((\d{2})\.)?/(\d{2})\.(\d{2})\.(\d{3,4})$')
def __99_99__99_99_9opt999__(match, instance):
    end_dd = int(match.group(4))
    end_mm = int(match.group(5))
    end_yy = int(match.group(6))

    start_dd = int(match.group(1))
    start_mm = int(match.group(3)) if match.group(3) else end_mm
    start_yy = end_yy

    diff = (date(end_yy, end_mm, end_dd) - date(start_yy, start_mm, start_dd)).days
    instance.set_attribute('day',   "{:>02}".format(start_dd))
    instance.set_attribute('month', "{:>02}".format(start_mm))
    instance.set_attribute('year',  "{:>04}".format(start_yy))
    instance.set_attribute('type',  'certain')
    instance.set_attribute('size',  str(1 + diff))
    instance.set_attribute('unit',  'day')
    instance.set_attribute('mode',  'after')


#   Mitte des 4. Jh.
#   Mitte des 14. Jh.
@RegexBagBuilder.using(default_date_instance, r'^Mitte des (\d{1,2})\. Jh\.$')
def __mitte_des_99_Jh__(match, instance):
    year = (int(match.group(1)) - 1) * 100 + 37
    diff = 26
    instance.set_attribute('year', "{:<02}".format(year))
    instance.set_attribute('size', str(diff))
    instance.set_attribute('unit', 'year')
    instance.set_attribute('type', 'certain')


#  Must be the last one, since it contains a generic pattern [\w\.]+
#
#   März 1429
#   Ende November 1521
@RegexBagBuilder.using(default_date_instance, r'^(Ende )?([\w\.]+) (\d{1,4})$')
def __EndeOpt_month__9999__(match, instance):
    try:
        month = MONTH_NUMBER[match.group(2)]
    except KeyError:
        msg = "unrecognized month in '{}'"
        raise RuntimeError(msg.format(match.group(0)))

    num_days = monthrange(int(match.group(3)), month)[1]
    if match.group(1) is None:
        instance.set_attribute('year',  "{:>04}".format(match.group(3)))
        instance.set_attribute('month', "{:>02}".format(month))
        instance.set_attribute('day',   '01')
        instance.set_attribute('type',  'certain')
        instance.set_attribute('size',  num_days)
        instance.set_attribute('unit',  'day')
        instance.set_attribute('mode',  'after')
    else:
        this_date = date(int(match.group(3)), month, 1)
        next_month = this_date + timedelta(num_days)
        instance.set_attribute('year',  "{:>04}".format(next_month.year))
        instance.set_attribute('month', "{:>02}".format(next_month.month))
        instance.set_attribute('day',   '01')
        instance.set_attribute('type',  'expected')
        instance.set_attribute('size',  '10')
        instance.set_attribute('unit',  'day')
        instance.set_attribute('mode',  'before')


def ssrq_dates_parser():
    """Returns the histHub parser for SSRQ dates."""
    return RegexBagBuilder((
        __9opt999__,
        __9opt9opt_99_9999__,
        __um_9opt999__,
        __vor_9999__,
        __vor_99_99_9999__,
        __99_Jh__,
        __nach_9999__,
        __ca_9999__,
        __vor_99_9999__,
        __9999_999opt9opt__,
        __deja_mort_en_9999__,
        __prob_9999__,
        __99quest99__,
        __nach_99_99_9999__,
        __9opt999quest__,
        __9999_quest__,
        __zwischen_9999_und_9999__,
        __recement_mort_9999__,
        __cite_en_9opt999__,
        __9999er_jahre__,
        __anfang_99_Jh__,
        __9_haelfte_99_Jh__,
        __99_oder_99_Jh__,
        __frsp_9999__,
        __anfangs_mitte_ende_9opt999__,
        __um_99_99_9opt999__,
        __9999_oder_9999__,
        __99_99__99_99_9opt999__,
        __mitte_des_99_Jh__,

        # must be the last one
        __EndeOpt_month__9999__,
        ))


if __name__ == '__main__':
    separator = 50 * '-'
    date_parser = ssrq_dates_parser()

    from sys import stdin
    for line in stdin:
        try:
            free_text = line[0:-1]
            print("INPUT '{}'".format(free_text))
            print(separator)
            print(date_parser.parse(free_text))
            print("\n")
        except Exception as e:
            from traceback import print_exc
            print_exc()
            print("ERROR: {}".format(e))
