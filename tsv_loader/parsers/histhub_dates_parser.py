from eavlib.core.eav_bag import EavBag
from .ssrq_dates_parser import ssrq_dates_parser
from .iso_8601_dates_parser import iso_8601_dates_parser


class HistHubDatesParser:
    def __init__(self):
        self._iso_8601_parser = iso_8601_dates_parser()
        self._ssrq_dates_parser = ssrq_dates_parser()

    def parse(self, free_text, params=None):
        """Reads free text as input and returns a list of EAV ForenamePart
        bags.
        """
        note = EavBag('Note')
        note.set_attribute('text', free_text)

        # Try to parse a date as a ISO-8601 one.
        try:
            date_instance = self._iso_8601_parser.parse(free_text)
            date_instance.add_related('notes', note)
            return date_instance
        except RuntimeError:
            pass

        # If not successful, try to use the SSRQ conventions.
        try:
            date_instance = self._ssrq_dates_parser.parse(free_text)
            date_instance.add_related('notes', note)
            return date_instance
        except RuntimeError:
            err_msg = "cannot parse '{}' as a date"
            raise RuntimeError(err_msg.format(free_text))


def histhub_dates_parser():
    """Returns the histHub parser for dates."""
    return HistHubDatesParser()


if __name__ == '__main__':
    separator = 50 * '-'
    dates_parser = histhub_dates_parser()

    from sys import stdin
    for line in stdin:
        try:
            free_text = line[0:-1]
            print(separator)
            print("INPUT '{}'".format(free_text))
            date_bag = dates_parser.parse(free_text)
            print(date_bag)
        except Exception as e:
            #from traceback import print_exc
            #print_exc()
            print("ERROR: {}".format(e))
    print(separator)
