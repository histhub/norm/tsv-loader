import re

from eavlib.core.eav_bag import EavBag

from .histhub_dates_parser import histhub_dates_parser


class HistHubDurationsParser:
    """
        'ab 1424'
            is turned into 1424- and processed in this form.

        'ab 1424 mehrfach'
            The term mehrfach is dropped (as it doesn't convey any meaning or
            information), the date is turned into 1424- and processed in this
            form.
    """

    # Several durations can be expressed in a list: 1431-1446, 1450, 1459-1461
    DURATION_LIST_SEPARATOR = ','

    ##
    #  Each duration might be expressed by two dates separated by a hyphen (or
    #  hyphen like character).
    #
    #     1914-1918
    #     2017-
    #     -1258
    #     1923―1978
    #
    DURATION_ENDS_SEPARATOR = '-'

    def __init__(self):
        self._dates_parser = histhub_dates_parser()

        ###
        ##  With kind regards to https://www.regextester.com/
        #
        self._ab_regex = re.compile('^ab ((.(?!mehrfach$))*)')
        self._bis_regex = re.compile('^bis(?: zur Entlassung)? ([0-9]+)$')

    def parse(self, free_text, params=None):
        """Reads free text as input and returns a list of EAV Duration bags."""
        if not free_text:
            raise RuntimeError('empty input')

        # Flatten Unicode dashes
        input_text = free_text.translate({
            0x2010: self.DURATION_ENDS_SEPARATOR,  # Hyphen
            0x2012: self.DURATION_ENDS_SEPARATOR,  # Figure Dash
            0x2013: self.DURATION_ENDS_SEPARATOR,  # En Dash
            0x2014: self.DURATION_ENDS_SEPARATOR,  # Em Dash
            0x2015: self.DURATION_ENDS_SEPARATOR,  # Horizontal Bar
            0x003B: self.DURATION_LIST_SEPARATOR,  # ';' -> ',' for some wrong usage of ';'
            })

        duration_bags = []

        # Split for durations
        durations_list = input_text.split(self.DURATION_LIST_SEPARATOR)
        for element in durations_list:
            duration_text = element.strip()

            # Two pathological uses: 'ab 1543' and 'bis 1234' have to be turned
            # into a start of duration or end of duration (appending or
            # prepending a hyphen).
            ab_match = self._ab_regex.match(duration_text)
            if ab_match:
                duration_text = "{}{}".format(
                    duration_text.replace('mehrfach', '').strip(),
                    self.DURATION_ENDS_SEPARATOR)

            bis_match = self._bis_regex.match(duration_text)
            if bis_match:
                duration_text = "{}{}".format(
                    self.DURATION_ENDS_SEPARATOR, duration_text.strip())

            # Split for start and end dates
            dur_parts = duration_text.split('-')
            start_bag = None
            end_bag = None

            if len(dur_parts) == 1:
                # Although the same date, two different objects
                # are returned so that no cycles are generated.
                start_bag = self._dates_parser.parse(dur_parts[0].strip())
                from copy import deepcopy
                end_bag = deepcopy(start_bag)
                #end_bag = self._dates_parser.parse(dur_parts[0].strip())
            elif len(dur_parts) == 2:
                start_date = dur_parts[0].strip()
                end_date = dur_parts[1].strip()
                if start_date:
                    start_bag = self._dates_parser.parse(dur_parts[0].strip())
                if end_date:
                    end_bag = self._dates_parser.parse(dur_parts[1].strip())
            else:
                msg = "wrong dates separation in '{}'"
                raise RuntimeError(msg.format(duration_text))

            note = EavBag('Note')
            note.set_attribute('text', element.strip())

            duration = EavBag('DateInterval')
            duration.add_related('notes', note)
            if start_bag:
                duration.add_related('start', start_bag)
            if end_bag:
                duration.add_related('end', end_bag)

            duration_bags.append(duration)

        return duration_bags


def histhub_durations_parser():
    """Returns the histHub parser for SSRQ durations."""
    return HistHubDurationsParser()


if __name__ == '__main__':
    separator = 50 * '-'
    du_parser = histhub_durations_parser()

    from sys import stdin
    for line in stdin:
        try:
            free_text = line[0:-1]
            print("INPUT '{}'".format(free_text))
            print(separator)
            for duration in du_parser.parse(free_text):
                print(duration)
                print(separator)
            print("\n")
        except Exception as e:
            #from traceback import print_exc
            #print_exc()
            print("ERROR: {}".format(e))
