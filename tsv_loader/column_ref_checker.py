from tsv_loader.load_plan_visitor import LoadPlanVisitor


class ColumnReferenceChecker(LoadPlanVisitor):
    """Visitor to check that the column number used in a load plan are not out
    of bounds.
    """

    def __init__(self, max_col):
        self.errors = []
        self._col_number_limit = max_col
        self._current_file = None

    def _error(self, fmt, *args):
        self.errors.append(
            "spec for file '%s': out of bounds column number in %s" % (
                self._current_file, fmt.format(*args)))

    def _out_of_bounds(self, col_number):
        return col_number <= 0 or self._col_number_limit < col_number

    def _check_as_cache_ref(self, class_name, col_number):
        if self._out_of_bounds(col_number):
            self._error("cache reference '{}[{}]'", class_name, col_number)

    def _check_as_database_ref(self, class_name, col_number):
        if self._out_of_bounds(col_number):
            self._error("database reference '{}<{}>'", class_name, col_number)

    def _check_as_condition(self, col_number):
        if self._out_of_bounds(col_number):
            self._error("condition 'CASE {}'", col_number)

    def _check_as_attribute(self, attr_name, col_number):
        if self._out_of_bounds(col_number):
            self._error(
                "attribute assignment '{} = {}'", attr_name, col_number)

    def _check_as_parsed(self, class_name, col_number):
        if self._out_of_bounds(col_number):
            self._error(
                "parse expression '{} {{ PARSE {}'", class_name, col_number)

    ###################     Load Plan Visitor Interface     ###################

    def _previsit_file_spec(self, file_spec):
        self._current_file = file_spec.filename

    def _previsit_instance_rule(self, rule):
        if rule.cache_key and type(rule.cache_key) is int:
            self._check_as_cache_ref(rule.class_name, rule.cache_key)

    def _previsit_condition(self, rule):
        self._check_as_condition(rule.col_number)

    def _previsit_cached_parent_rule(self, rule):
        if type(rule.cache_key) is int:
            self._check_as_cache_ref(rule.class_name, rule.cache_key)

    def _previsit_attribute_rule(self, rule):
        if type(rule.value_spec) is int:
            self._check_as_attribute(rule.attribute_name, rule.value_spec)

    def _previsit_cached_target_rule(self, rule):
        if type(rule.cache_key) is int:
            self._check_as_cache_ref(rule.class_name, rule.cache_key)

    def _previsit_database_target_rule(self, rule):
        if type(rule.database_ref) is int:
            self._check_as_database_ref(rule.class_name, rule.database_ref)

    def _previsit_parse_target_rule(self, rule):
        self._check_as_parsed(rule.class_name, rule.col_number)

    def _previsit_cached_child_rule(self, rule):
        if type(rule.cache_key) is int:
            self._check_as_cache_ref(rule.class_name, rule.cache_key)
