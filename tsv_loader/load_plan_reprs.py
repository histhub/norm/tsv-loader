# TODO: some consistency checks are missing:
#        - A Linking rule must have a CachedParentRule
#        - A NormalizingRule must never have a CachedParentRule
# TODO: LineActionRule might only have condition, but not CachedParentRule

from itertools import zip_longest


def pprint(load_spec, indent=0):
    def shitf(value, indent, num=1):
        reindented = pprint(value, indent)
        if indent:
            newl = "\n{}".format(num*indent*' ')
            reindented = newl.join(reindented.split('\n'))
        return reindented

    def value_repr(value, indent):
        if isinstance(value, str):
            return '"{}"'.format(value)
        elif isinstance(value, LoadPlanRepr):
            return shitf(value, indent)
        elif isinstance(value, (set, list)):
            if not value:
                return "[]"
            else:
                prefix = "[\n{}".format(2*indent*' ') if indent else '['
                suffix = "\n{}]".format(indent*' ') if indent else ']'
                litems = []
                for v in value:
                    if isinstance(v, LoadPlanRepr):
                        litems.append(shitf(v, indent, 2))
                    else:
                        litems.append(value_repr(v, indent))
                sep = ",\n{}".format(2*indent*' ') if indent else ','
                return ''.join((prefix, sep.join(litems), suffix))
        else:
            return str(value)

    start, end = "{}(".format(load_spec.__class__.__name__), ')'
    prop_texts = list()
    assign = ' = ' if indent else '='
    for p in load_spec.__slots__:
        value = getattr(load_spec, p)
        text = ''.join((indent*' ', p, assign, value_repr(value, indent)))
        prop_texts.append(text)
    pjoin = '\n' if indent else ','
    return ('\n' if indent else '').join((start, pjoin.join(prop_texts), end))


class LoadPlanRepr:
    """Superclass for classes representing the TSV load plan parser's output"""

    def __repr__(self):
        return pprint(self, 0)

    def __str__(self):
        return repr(self)

    def __init__(self, *values):
        for k, v in zip_longest(self.__slots__, values, fillvalue=None):
            setattr(self, k, v)

    class StopTraversal(Exception):
        """Used by a "visitor" to stop a traversal started with
        traverse_with.
        """
        pass

    def traverse_with(self, visitor):
        """Standard way to traverse the web of objects that make up a load plan
        representation. The passed visitor specifies the traversal sought
        behaviour. A visitor can call the _stop_traversal() method to abort it.
        """
        try:
            self._traverse_with(visitor)
        except LoadPlanRepr.StopTraversal:
            pass
        except Exception:
            raise

    def _traverse_with(self, visitor):
        visitor.previsit(self)
        self._traverse_children(visitor)
        visitor.postvisit(self)

    # TODO this might be done inspecting the __slots__
    def _traverse_children(self, visitor):
        if isinstance(self, TsvLoadSpec):
            if self.params:
                self.params._traverse_with(visitor)
            for file_spec in self.file_specs:
                file_spec._traverse_with(visitor)

        elif isinstance(self, FileSpec):
            for rule in self.line_rules:
                rule._traverse_with(visitor)

        elif isinstance(self, LineActionRule):
            if self.condition:
                self.condition._traverse_with(visitor)
            if self.cached_parent:
                self.cached_parent._traverse_with(visitor)

            if isinstance(self, BuildingRule):
                self.built_child._traverse_with(visitor)
            if isinstance(self, LinkingRule):
                self.cached_child._traverse_with(visitor)

        elif isinstance(self, InstanceRule):
            for attr_rule in self.attribute_rules:
                attr_rule._traverse_with(visitor)
            for rel_rule in self.relation_rules:
                rel_rule._traverse_with(visitor)

        elif isinstance(self, BuildTargetRule):
            self.built_child._traverse_with(visitor)

        elif isinstance(self, ParseTargetRule):
            if self.params:
                self.params._traverse_with(visitor)

        elif isinstance(self, (
                LoadParams,
                Condition,
                CachedParentRule,
                AttributeRule,
                CachedTargetRule,
                DatabaseTargetRule,
                CachedChildRule)):
            pass  # terminals

        elif isinstance(self, (
                RelationRule,
                LoadPlanRepr)):
            pass  # abstract


class TsvLoadSpec(LoadPlanRepr):
    """Represents the information contained in a load plan file"""
    __slots__ = ('params', 'file_specs', 'directory')


class LoadParams(LoadPlanRepr):
    __slots__ = ('file_format', 'skip_header', 'drop_quotes')


class FileSpec(LoadPlanRepr):
    """Represents instructions to load information contained in a single TSV
    file.
    """
    __slots__ = ('filename', 'line_rules')


class LineActionRule(LoadPlanRepr):
    """Abstract class for any action that can be done with a line of a TSV
    file.
    """
    __slots__ = ('condition', 'cached_parent')


class BuildingRule(LineActionRule):
    """Action for building objects using information from fields of a TSV
    line.
    """
    __slots__ = LineActionRule.__slots__ + ('built_child',)


class LinkingRule(LineActionRule):
    """Action for connection two objects through a (n-to-m) relation"""
    __slots__ = LineActionRule.__slots__ + ('cached_child',)


class NormalizingRule(LineActionRule):
    """Action for storing normalized values and their variants"""
    __slots__ = LineActionRule.__slots__ + ('variant', 'standard')


class Condition(LoadPlanRepr):
    """Expression for conditional execution os a action rule"""
    __slots__ = ('col_number', 'allowed_values', 'is_negated')


class CachedParentRule(LoadPlanRepr):
    """Rule to retrieve from the cache parent objects that the object being
    built is related to. If cache_key is int, the key is the content of the
    column with that number, otherwise cache key is itself the key.
    """
    __slots__ = ('relation_name', 'class_name', 'cache_key')


class InstanceRule(LoadPlanRepr):
    """Rule for building an object from specifications of its attributes and
    childen objects. If cache_key is int, the key is the content of the
    column with that number, otherwise cache key is itself the key.
    """
    __slots__ = ('class_name', 'cache_key', 'attribute_rules',
                 'relation_rules')


class AttributeRule(LoadPlanRepr):
    """Rule for building attributes. Wenn value is an integer, it refers to a
    column and the attribute's value is the content of that column; otherwise
    the attribute gets what is in value.
    """
    __slots__ = ('attribute_name', 'is_optional', 'is_normalized',
                 'value_spec')


class RelationRule(LoadPlanRepr):
    """Abstract base class for rules describing children objects"""
    __slots__ = ('relation_name', 'is_optional')


class BuildTargetRule(RelationRule):
    """Relation rule where the target object is built using information from
    fields of a TSV line.
    """
    __slots__ = RelationRule.__slots__ + ('built_child',)


class CachedTargetRule(RelationRule):
    """Relation rule for target objects retrieved from a cache where they where
    previuosly loaded. If cache_key is int, the key is the content of the
    column with that number, otherwise cache key is itself the key.
    """
    __slots__ = RelationRule.__slots__ + ('class_name', 'cache_key')


class DatabaseTargetRule(RelationRule):
    """Relation rule where the target object is specified by a database ID.  If
    database_ref is int, the database ID is in that line's column, otherwise
    database_ref is itself the database ID.
    """
    __slots__ = RelationRule.__slots__ + ('class_name', 'database_ref')


class ParseTargetRule(RelationRule):
    """Relation rule for target objects built by parsing information from a
    line.
    """
    __slots__ = RelationRule.__slots__ + (
        'class_name', 'col_number', 'parser_name', 'params')


class CachedChildRule(LoadPlanRepr):
    """Rule to retrieve from the cache an object that will be a child in an
    n-to-m link. If cache_key is int, the key is the content of the column with
    that number, otherwise cache key is itself the key.
    """
    __slots__ = ('class_name', 'cache_key')
