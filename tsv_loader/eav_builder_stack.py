import logging

LOG = logging.getLogger(__name__)


class EavBuilderStack:
    """Helping stack used by a LineRuleEavBuilder."""

    class Record:
        __slots__ = ('eav_node', 'relation', 'is_optional', 'parse_errors',
                     'missed_values')

    def __init__(self):
        self._stack = []

    def clear(self):
        self._stack.clear()

    def top_record(self):
        return self._stack[-1]

    def push_record(self, relation_name=None, is_optional=False):
        new_record = EavBuilderStack.Record()
        new_record.eav_node = None
        new_record.relation = relation_name
        new_record.is_optional = is_optional
        new_record.parse_errors = list()
        new_record.missed_values = list()
        self._stack.append(new_record)

    def pop_record(self):
        if len(self._stack) > 1:
            previous_top = self.top_record()
            self._stack.pop()

            # Records without relation are used to build parsing parameters;
            # no need to relate build object or transfer information down
            if previous_top.relation is None:
                return

            if not previous_top.missed_values:
                node_on_top = previous_top.eav_node
                self.set_target(previous_top.relation, node_on_top)

            if previous_top.is_optional and previous_top.missed_values:
                # Information about missing values found in objects
                # downstream is forgotten when hanging from a upper
                # optional relation.
                previous_top.missed_values.clear()

            self.top_record().parse_errors.extend(previous_top.parse_errors)
            self.top_record().missed_values.extend(previous_top.missed_values)

    def set_node(self, eav_node):
        """Set the EAV working node in the top of the stack"""
        self.top_record().eav_node = eav_node

    def set_attribute(self, attribute_name, value):
        """Set attribute to the EAV node in the top of the stack"""
        self.top_record().eav_node.set_attribute(attribute_name, value)

    def set_target(self, relation_name, target_object):
        """Adds a target object to the set of related objects of the EAV node
        in the top of the stack.
        """
        self.top_record().eav_node.add_related(relation_name, target_object)

    def append_missed_value(self, missed_value):
        self.top_record().missed_values.append(missed_value)

    def append_parse_error(self, parse_error):
        self.top_record().parse_errors.append(parse_error)

    def path_to(self, last):
        first = self._stack[0].eav_node.entity_name
        middle = '.'.join(r.relation for r in self._stack[1:])
        return "{}.{}.{}".format(first, middle, last)

    def dump(self):
        LOG.debug(90*'=')
        for num, record in enumerate(self._stack):
            LOG.debug("[ %d ]", num)
            LOG.debug("relation      = %s", record.relation)
            LOG.debug("is_optional   = %s", record.is_optional)
            LOG.debug("missed_values = [ %s ]", ', '.join(record.missed_values))
            LOG.debug("parse_errors  = [ %s ]", ', '.join(record.parse_errors))
            LOG.debug("eav_node      = %s",
                      str(record.eav_node).replace('\n', ' ')
                      if record.eav_node else None)
