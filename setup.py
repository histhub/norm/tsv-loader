#!./venv/bin/python

import codecs
import os

from setuptools import setup, find_packages

package_name = "tsv_loader"
min_python_version = (3, 4)

base = os.path.abspath(os.path.dirname(__file__))

try:
    from pip._internal.req import parse_requirements
except ImportError:
    try:
        from pip.req import parse_requirements
    except ImportError:
        raise RuntimeError("Could not find pip requirements parser")


def read_file(path):
    """Read file and return contents"""
    try:
        with codecs.open(path, "r", encoding="utf-8") as f:
            return f.read()
    except IOError:
        return ""


def get_reqs(target):
    """Parse requirements.txt files and return array"""
    reqs = parse_requirements(
        os.path.join(base, "requirements.d", ("%s.txt" % target)),
        session="hack")

    return [
        str(r.requirement if hasattr(r, "requirement") else r.req)
        for r in reqs]


def get_meta(name):
    """Get metadata from project's __init__.py"""
    return getattr(__import__(package_name), name)


setup(
    name=package_name,
    version=get_meta("__version__"),

    description=get_meta("__description__"),
    long_description=read_file(os.path.join(base, "README.md")),

    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],

    install_requires=get_reqs("app"),
    extras_require=dict(
        dev=get_reqs("development")
    ),

    packages=find_packages(),
    python_requires=(">=%s" % ".".join(map(str, min_python_version))),
    scripts=[("bin/%s" % script) for script in (
        "eav_load_tsv",
    )])
